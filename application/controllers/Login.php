<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	 
	private $theme; 
	 
	public function __construct()
	{
		parent::__construct();

		## asset config	
		$theme = $this->config->item('theme');
		$this->theme = $theme; 

		$this->asset_url = $this->config->item('asset_url');
		$this->js_url = $this->config->item('js_url');
		$this->css_url = $this->config->item('css_url');
		$this->image_url = $this->config->item('image_url');

		$this->img_path = $this->image_url;

		$this->template->write('js_url', $this->js_url);
        $this->template->write('css_url', $this->css_url);
		$this->template->write('asset_url', $this->asset_url);
		$this->template->write('image_url', $this->image_url);
		// ini_set('display_errors', 1);
		// error_reporting(E_ALL);

	}
	 
	public function index()
	{

		$this->backoffice_model->checksession();
		        
					
	}
	public function Account() {
		$setTitle = strtoupper($this->router->fetch_method().' '.$this->router->fetch_class());
		
		$this->template->set_master_template('themes/'. $this->theme .'/tpl_login.php');
		$this->template->write('page_title', 'TBKK | '.$setTitle.'');
		$this->template->write_view('page_content', 'themes/'. $this->theme .'/view_login.php');
		$this->template->render();
	}
 public function Test(){
	  $username =$_POST["username"];
	  $password =$_POST["password"];

	$resLogin =$this->backoffice_model->modelCheckLogin($username,$password);
	// redirect('Login/Account');	 กลับหน้า Login
	if(empty($resLogin)){
		redirect('Login/Account');
	}else{
		redirect('Login/Home');
	}

}
	public function Home(){
		$data["resultUser"] = $this->backoffice_model->modelGetuser();
		$setTitle = strtoupper($this->router->fetch_method().' '.$this->router->fetch_class());
		$this->template->set_master_template('themes/'. $this->theme .'/tpl_Home.php');
		$this->template->write('page_title', 'TBKK | '.$setTitle.'');
		$this->template->write_view('page_content', 'themes/'. $this->theme .'/tpl_Home.php',$data);
		$this->template->render();
	}
	public function Edituser(){
		$id = $_GET["id"];
		$data["result_user"] = $this->backoffice_model->modelGetUserById($id);
		$setTitle = strtoupper($this->router->fetch_method().' '.$this->router->fetch_class());
		$this->template->set_master_template('themes/'.$this->theme .'/tpl_EditUser.php');
		$this->template->write('page_title', 'TBKK | '.$setTitle.'');
		$this->template->write_view('page_content','themes/'.$this->theme .'/tpl_EditUser.php',$data);
		$this->template->render();
		
		
		
	}
	public function upDateUser(){
		$id = $_POST["id"];
		$username =$_POST["username"];
		$pass=$_POST["pass"];
		$res = $this->backoffice_model->modelUpdateUser($id,$username,$pass);

	    if($res){
			redirect("Login/Home");
		}else{
			echo "Update Faill";
		}
	}
	public function deleteUser(){
		$id = $_GET["id"];
		$res = $this->backoffice_model->modelDelUser($id);
		if($res){
		 redirect("Login/Home");
		}else{
		 echo "Delete Fail";
		}
		// $setTitle = strtoupper($this->router->fetch_method().' '.$this->router->fetch_class());
		// $data["resulr_user"] = $this->backoffice_model->modelDelUser($id);

		// echo "------------------------>>> $id";
		// $this->template->set_master_template('themes/'.$this->theme .'/tpl_edit.php');
		// $this->template->write('page_title', 'TBKK | '.$setTitle.'');
		// $this->template->write_view('page_content','themes/', $this->theme .'/tpl_edit.php' , $data);
		// $this->template->render();
	}
 

 public function testInsert(){
	
	echo $regisname =$_POST["regisname"];
	echo $regispass =$_POST["regispass"];
	
	
	//$data =$_POST($username,$password);
	$resLogin =$this->backoffice_model->modelCheckInsert($regisname,$regispass);
	print_r($resLogin);


 }

	public function Account_BK()
	{
		$this->backoffice_model->CheckSsadsaesssion();

		$data['str_validate'] = '';
		$data['img_path'] = $this->img_path;

		$action = base64_decode($this->input->post('action'));

		if ($action == 'login'){
			
			$this->load->helper('security'); 
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger fade in">
								<button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-times"></i>
								<strong>Error!</strong><br />', '</div>');
			$this->form_validation->set_rules('txt_usr', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('txt_pwd', 'Password', 'trim|required|alpha_numeric|xss_clean');
			
			$this->form_validation->set_message('required', '%s ไม่มีข้อมูล กรุณาทำการตรวจสอบด้วยค่ะ');
			$this->form_validation->set_message('alpha_numeric', '%s ห้ามใช้ตัวอักษรอักขระพิเศษ'); 
			
			if ($this->form_validation->run() == FALSE){
				
				$data['str_validate'] = FALSE;
												
			}else{ 

				$usr = $this->input->post('txt_usr');
				$pwd = $this->input->post('txt_pwd');
				$usrData = $this->backoffice_model->Login($usr,$pwd);
								
				if($usrData['action'] != 'err') {
										
					$arrData = array('sessName'=> $usrData['firstname'], 'sessUsr'=> $usrData['username'], 'sessUsrId'=>$usrData['su_id'],'sessGroup'=>$usrData['sug_id'], 'sessLastacc'=> $usrData['last_access'], 'loggedIn' => "OK");				
					
					$this->session->set_userdata($arrData);
					redirect('login');


					

																		
				} else {
					
					if($usrData['value']=='b'){

						$this->session->set_flashdata('msg_error','<div class="alert alert-danger fade in">
								<button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-times"></i>
								<strong>Error!</strong><br />แอคเคาท์นี้ถูกระงับ<br />Account is baned.</div>');
						
					}else{

						$this->session->set_flashdata('msg_error','<div class="alert alert-danger fade in">
								<button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-times"></i>
								<strong>Error!</strong><br />รหัสผ่านไม่ถูกต้อง กรุณาทำการตรวจสอบข้อมูลอีกครั้งค่ะ <br />Invalid Account : Please check your account correctly.</div>');
					
					}
					redirect('login/account');
					
				}

			}
		}

		
			
	}
	
}

?>