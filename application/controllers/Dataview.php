<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dataview extends CI_Controller {

	private $theme; 
		 
	public function __construct()
	{
		parent::__construct();


		## asset config
		$theme = $this->config->item('theme');
		$this->theme = $theme; 

		$this->asset_url = $this->config->item('asset_url');
		$this->js_url = $this->config->item('js_url');
		$this->css_url = $this->config->item('css_url');
		$this->image_url = $this->config->item('image_url');

		$this->img_path = $this->image_url;

		$this->template->write('js_url', $this->js_url);
        $this->template->write('css_url', $this->css_url);
		$this->template->write('asset_url', $this->asset_url);
		$this->template->write('image_url', $this->image_url);

	}

	public function index(){
		$mindate = $_POST['minDate'];
		$maxdate = $_POST['maxDate'];
		redirect('Dataview/manage/'.$mindate.'/'.$maxdate);
	}


    public function manage(){

		$data['str_validate'] = '';		
		$checkSess = $this->backoffice_model->CheckSession();
		$this->backoffice_model->CheckPermission($this->session->userdata('sessUsrId'));
        // $data['list_qc'] = $this->backoffice_model->qc_data();
		// $data['mindate'] = '';
		// $data['maxdate'] = '';
		// $action = base64_decode($this->input->post('action'));
		// $checkmin = $this->uri->segment(3);
		// $checkmax = $this->uri->segment(4);

		// if($checkmax != '' or $checkmin != ''){
		// 	 $data['mindate'] = $this->uri->segment(3);
		// 	 $data['maxdate'] = $this->uri->segment(4);
		// 	if ($checkmax == ''){$checkmax = date('Y-m-d');}
		// 	$data['list_qc'] = $this->backoffice_model->qc_selectdate($checkmin, $checkmax);
		// }	
		// else{
		// 	$data['list_qc'] = $this->backoffice_model->qc_data();
        // }
        
		$setTitle = strtoupper($this->router->fetch_method().' '.$this->router->fetch_class());	
		
		$this->template->write('page_title', 'TBKK | '.$setTitle.'');
		$this->template->write_view('page_header', 'themes/'. $this->theme .'/view_header.php', $data);
		$this->template->write_view('page_menu', 'themes/'. $this->theme .'/view_menu.php');
		$this->template->write_view('page_content', 'themes/'. $this->theme .'/view_dataview.php', $data);
		$this->template->write_view('page_footer', 'themes/'. $this->theme .'/view_footer.php');
		$this->template->render();

	}
	
	
    
    public function showfrom_date(){

		$data['str_validate'] = '';
		$checkSess = $this->backoffice_model->CheckSession();
        $this->backoffice_model->CheckPermission($this->session->userdata('sessUsrId'));
        $p['input'] = $this->input->post('datepick');
        $data['list_qc'] = $this->backoffice_model->qc_selectdate($p);
        if (!empty($data['list_qc'])){
            $data['list_qc'];
        }
        else{
            redirect('dataview/manage');
            
        }
        
        
        

		$setTitle = strtoupper($this->router->fetch_method().' '.$this->router->fetch_class());	
		
		$this->template->write('page_title', 'TBKK | '.$setTitle.'');
		$this->template->write_view('page_header', 'themes/'. $this->theme .'/view_header.php', $data);
		$this->template->write_view('page_menu', 'themes/'. $this->theme .'/view_menu.php');
		$this->template->write_view('page_content', 'themes/'. $this->theme .'/view_dataview.php', $data);
		$this->template->write_view('page_footer', 'themes/'. $this->theme .'/view_footer.php');
		$this->template->render();
	}

	public function edit(){

		$checkSess = $this->backoffice_model->CheckSession();
        //$this->backoffice_model->CheckPermission($this->session->userdata('sessUsrId'));
		$data = $_POST['data'];
		$id = $_POST['qcid'];

		$this->backoffice_model->editqcdata($data, $id);
		
		redirect('Dataview/manage/');
	}


	public function delete_qc_detail($qcid){
		$checkSess = $this->backoffice_model->CheckSession();
		$result = $this->backoffice_model->deleteqcdetail($qcid);
		
		if ($result!=False) {
		
			$this->session->set_flashdata('msgResponse', '<div class="alert alert-success fade in">
								<button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-check"></i> <strong>ลบข้อมูลเรียบร้อยค่ะ </strong><br />Success : Delete data success.</div>');
			redirect('Dataview/manage');
		}else{
			$this->session->set_flashdata('msgResponse','<div class="alert alert-danger fade in">
								<button class="close" data-dismiss="alert">
									×
								</button>
								<i class="fa-fw fa fa-times"></i>
								<strong>Error!</strong><br />เกิดข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ค่ะ <br />Error : Save data not found.</div>');
			redirect('Dataview/manage');
		}

	}

	public function checkall_data(){
		
		$checkSess = $this->backoffice_model->CheckSession();
		$this->backoffice_model->CheckPermission($this->session->userdata('sessUsrId'));

		$qcid = $this->input->post('chk_uid');
		//$qcid = $this->input->post('chk_qcid');
		$this->backoffice_model->num_check_data($qcid);

		$this->session->set_flashdata('msgResponse', '<div class="alert alert-success fade in"><button class="close" data-dismiss="alert">×</button><i class="fa-fw fa fa-check"></i> <strong>ตรวจสอบข้อมูลเรียบร้อยค่ะ </strong><br />Success : Enable data success.</div>');
		redirect('Dataview/manage/');
	}


	public function testformatjson(){
		header('Content-Type: application/json');
		$result["data"] = $this->backoffice_model->test_json();
		

		echo json_encode($result);
	}

	public function getName(){
		header('Content-Type: application/json');
		$usrid = $_POST['id'];
		$result = $this->backoffice_model->getname($usrid);
		
		echo json_encode($result);
	}

	public function data_server(){
		header('Content-Type: application/json');
		$mindate = $_GET['minDate'];
		$maxdate = $_GET['maxDate'];
		$phase = $_GET['phase'];

		$table = "qc_details";
		$primaryKey = "qc_id";
		
		$columns = array(
		array("db" => "qc_id", "dt" => 0),
		array("db" => "line_name", "dt" => 2),
		array("db" => "op_name", "dt" => 3),
		array("db" => "part_name", "dt" => 4),
		array("db" => "part_no", "dt" => 5),
		array("db" => "pro_no", "dt" => 6),
		array("db" => "item_no", "dt" => 7),
		array("db" => "shift", "dt" => 8),
		array("db" => "time", "dt" => 9),
		array("db" => "data", "dt" => 10),
		array("db" => "date_created","dt" => 11),
		array("db" => "qc_id","dt" => 12),
		array("db" => "emp_checked", "dt" => 13),
		array(
			'db' => 'leader_checked',
			'dt' => 14,
			'formatter' => function( $d, $row ) {
			  return $this->backoffice_model->getname($d);
			}
		  ),

		);
		
		$sql_details = array(
		"user" => "monty",
		"pass" => "some_pass",
		"db"   => "qc_system",
		"host" => "192.168.82.31"
		);
		//require( "ssp_with_UTF8.class.php" );
		require_once(APPPATH.'libraries/ssp.php');
		

		if($mindate != '' and $maxdate != ''){
			$where = "phase = '$phase' AND date(date_created) BETWEEN '$mindate' AND '$maxdate' AND del_flag = '0' ";
		}else{
		 	$where = "phase = '$phase' AND del_flag ='0' AND date_created >= NOW() - INTERVAL 1 DAY ";
		}
		$extra ='ORDER BY date_created DESC';
		
		//$where = "date(date_created) BETWEEN '2020-11-20' AND '2020-11-22'  ";
		$result = SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns, null, $where, $extra);

		$start=$_REQUEST['start'] + 1;
		$idx=1;
		foreach($result['data'] as &$res){
			$res[1]=(string)$start;
			$start++;
			$idx++;
		}
		

		echo json_encode($result);
	}

	public function testteung(){

		$data['str_validate'] = '';		
		$checkSess = $this->backoffice_model->CheckSession();

 
		$setTitle = strtoupper($this->router->fetch_method().' '.$this->router->fetch_class());	
		
		$this->template->write('page_title', 'TBKK | '.$setTitle.'');
		$this->template->write_view('page_header', 'themes/'. $this->theme .'/view_header.php', $data);
		
		$this->template->write_view('page_content', 'themes/'. $this->theme .'/view_test.php', $data);
		$this->template->write_view('page_footer', 'themes/'. $this->theme .'/view_footer.php');
		$this->template->render();

	}

	public function testjson(){
		header('Content-Type: application/json');
		
		$result["data"] = $this->backoffice_model->test_json();
		
		echo json_encode($result);
	}


}
