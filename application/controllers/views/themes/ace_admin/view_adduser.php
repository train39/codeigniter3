	<div class="main-content">
				<div class="main-content-inner">
					
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url().'manage/home/';?>">Home</a>
							</li>
							<li class="active">User</li>
							<li class="active">Add User</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>		
					

					<div class="page-content">
						<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->
						

								<div class="row">
									<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Add User</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										</div>






								<!-- PAGE CONTENT BEGINS -->

								<?php if($str_validate==FALSE){ echo validation_errors(); } ?>
								<?php if($this->session->flashdata('msgResponse') != ''){ echo $this->session->flashdata('msgResponse'); }?> 
								<?php echo form_open('user/add', array('id'=>'smart-form-register', 'class'=>'form-horizontal'));?>	
								<!-- <form class="form-horizontal" role="form"> -->
									<fieldset>
									<div class="form-group">
										
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Username </label>

										<div class="col-sm-9">
											
											<input type="text" name="txt_usr" class="col-xs-10 col-sm-5" placeholder="Username" value="<?php echo set_value('txt_usr'); ?>">
										</div>

									</div>
									
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Password </label>

										<div class="col-sm-9">
											<input type="password" name="txt_pwd" placeholder="Password" id="password" class="col-xs-10 col-sm-5" value="<?php echo set_value('txt_pwd'); ?>" />

										</div>
									</div>

									<div class="form-group">
									<label class="col-sm-3 control-label no-padding-right" > Group </label>
									<div class="col-sm-5">
									<label class="select">
										<?php
                                                        /*if ($g['del_flag'] == '0')*/

                                                        
								                        
								                        
								                        $optName = array();

								                        $optName['0'] = '-- Please Select Groups --';
								                        foreach($excLoadG->result() as $g){
								                        	if($g->{'del_by'} == '0');{
								                        	
								                        	

								                        	$optName[$g->sug_id] = $g->name;

								                        	}
								                        
								                        }
								                    
								                        $selected = (set_value('sel_group')) ? set_value('sel_group') : '-- Please Select Groups --';

								                        if(form_error('sel_group')){ 
								                        	$setErrSel = "class='err'"; }
								                        echo form_dropdown('sel_group', $optName,  $selected/*, $setErrSel*/);

								        ?>
									</label>
									</div class="col-sm-5">
								</div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" > Name </label>

										<div class="col-sm-5">
											<input name="txt_fname"  type="text" class="col-xs-10 col-sm-5"  placeholder="First name" value="<?php echo set_value('txt_fname'); ?>" />
										</div>
									</div>


									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" > Lastname </label>

										<div class="col-sm-5">
											<input name="txt_lname"  type="text" class="col-xs-10 col-sm-5"  placeholder="Last name" value="<?php echo set_value('txt_lname'); ?>" />
										</div>
									</div>

												<label class="col-sm-3 control-label no-padding-right" >Sex</label>
												<div class="inline-group">
												<div class="radio">
													
													<label>
														<input checked="checked" name="rad_sex" type="radio" class="ace" value="male" <?php echo set_radio('rad_sex', 'male', TRUE); ?>/>
														<span class="lbl"> Male</span>
													</label>
												
													<label>
														<input name="rad_sex" type="radio" class="ace" value="female" <?php echo set_radio('rad_sex', 'female'); ?>/>
														<span class="lbl"> Female</span>
													</label>
												</div>
											</div>
											<div class="space-8"></div>
											
								

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-4">Email</label>
										<div class="col-sm-5">

											<input name="txt_email" placeholder="E-mail" type="email" class="col-xs-10 col-sm-5" value="<?php echo set_value('txt_email'); ?> ">
											
											<div class="help-block" id="input-size-slider"></div>
										</div>
									</div>

									
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" type="submit">
												<i class="ace-icon fa fa-check bigger-110"></i>
												Submit
											</button>
										<input type="hidden" name="action" value="<?php echo base64_encode('addUser');?>"  />
									


<!-- 											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="ace-icon fa fa-undo bigger-110"></i>
												Reset
											</button> -->
										</div>
									</div>
									
						
								</fieldset>

								<?php echo form_close();?>
							</div>
						</div>
					</div>
						





									
