<?php 
	$setHeadContent = strtoupper(str_replace('_', ' ', $this->router->fetch_method()));
?>
<!-- //////////////////////////////////// CONTENT ////////////////////////////////////////////// -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url().'manage/home/';?>">Home</a>
							</li>

							<li class="active">Test</li>
							<li class="active">BOM Manage</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">
						<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->



						<!-- 								<div id="sel_item_exp" class="modal hide" > -->
							<div id="sel_item_exp" class="modal fade"  role="dialog">
								  <div class="modal-dialog">

								    <!-- Modal content-->
								    <div class="modal-content">
								      <div class="modal-dialog" >
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Please select Item Code
												</div>
											</div>

											<div class="modal-body no-padding" style="height: 400px; overflow: auto;">
												<!-- <div style="height: 400px; overflow: auto;"> -->
												<table class="table table-striped table-bordered table-hover no-margin-bottom no-border-top" id="table23" >
													<thead>
														<tr>
															<th style="text-align:center;" width="8%">No.</th>
															<th style="text-align:center;" width="12%">Select</th>
															<th style="text-align:center;" width="25%">Item Code</th>
															<th style="text-align:center;" width="30%">DENOMINATOR</th>
															<th style="text-align:center;" width="25%">NUMERATOR</th>
															

														</tr>
													</thead>

													<tbody>
													
													</tbody>
												</table>
												<!-- </div> -->
											</div>

											<div class="modal-footer no-margin-top">
												<button class="btn btn-sm btn-danger " data-dismiss="modal" name="back3" id="back3" onclick="clearselItemexp()">
													<i class="ace-icon fa fa-undo"></i>
													Cancel  
												</button>

												&nbsp; &nbsp; &nbsp;

												<button class="btn btn-sm btn-success pull-right" data-dismiss="modal" name="selitem" id="selitem" onclick="selItemexp()">
													<i class="ace-icon glyphicon glyphicon-ok"></i>
													Submit &nbsp;&nbsp;
												</button>

												
											</div>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div>
								</div>
								</div>


						<div class="row">
									<div class="col-xs-12">
										<h3 class="header smaller lighter blue">BOM Manage</h3>

										
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										</div>
						<div class="row">
							<div class="col-xs-12">



								<!-- PAGE CONTENT BEGINS -->
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<?php if ($this->session->flashdata('msgError') != ''){ echo $this->session->flashdata('msgError'); } ?>
				<?php if ($this->session->flashdata('msgResponse') != ''){ echo $this->session->flashdata('msgResponse'); } ?>
				</div>
				

				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-7">
				
					<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">



						<!-- widget div-->
						<div>
		
							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->
								
							</div>
							<!-- end widget edit box -->

							<!-- <div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div> -->
										<div class="table-header">
											List Items
										</div>	

									<!-- widget content -->
									<div class="widget-body no-padding">
										
										<?php echo form_open('#', array('id' => 'frm_permissiongroupmanagement', 'name'=>'	frm_permissiongroupmanagement'));?>

										<table id="dynamic-table" class="table table-striped table-bordered table-hover">
												<thead>
													
														
												<div class="clearfix">
													<div class="pull-right tableTools-container"></div>
												</div>
									
													</tr>
													<!-- <tr>
								                    	<td colspan="9">
									                    	<div id="btn_enable" class="btn btn-sm btn-success"><span class="fa fa-check-square-o"></span></div>
									                    	<div id="btn_disable" class="btn btn-sm btn-warning"><span class="fa fa-times"></span></div>
									                    	<div id="btn_delete" class="btn btn-sm btn-danger"><span class="fa fa-trash-o"></span></div>
								                    	</td>
								                    </tr> -->	
													<tr>
														<!-- <th class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>
														</th> -->
														<th>
															No.
														</th>
														<th>Comp Code</th>
														<th>Parent Code</th>
														<th>Denominator</th>
														<th>Numerator</th>
														<th style="text-align:center;">Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												<?php
													

													  $list_bom = array_filter($list_bom);
													 

											    	  if (!empty($list_bom)) {
													   
													  $i = 1;	  											  
													  foreach ($list_bom as $bom_detail){


														  
														  if ($bom_detail['enable'] == '1'){ 
														  
														  	$txt_status = '<span class="label label-sm label-success">Enable</span>'; 
														  	$txt_color = '#0EC952';
														  
														  }else{
															 
															$txt_status = '<span class="label label-sm label-warning">Disable</span>'; 
														  	$txt_color = '#FF0000'; 
															
														  }
												  
														 
														
													?>
												


												<tr>
													<!-- <td style="text-align:center;">
													 <label class="pos-rel">
																<input type="checkbox" class="ace" name="chk_itemcode" value="<?php echo $bom_detail['bom_id'];?>"/>
																<span class="lbl"></span>

														</label> 
													</td> -->
														<td style="text-align:center;"><?php echo $i;?></td>
														<td><?php echo $bom_detail['comp_code'];?></td>
														<td><?php echo $bom_detail['parent_code'];?></td>
														<td><?php echo $bom_detail['unit_denominator'];?></td>
														<!-- <td><?php echo $bom_detail['unit_numerator'];?></td> -->
														<td><?php echo number_format((float)$bom_detail['unit_numerator'], 2, '.', '');?></td>

														
														<td style="text-align:center;"><?php echo '<span style="color:'.$txt_color.'">'.$txt_status.'</span>';?></td>
														
														<td style="text-align:center;">

														<button type="button" class='btn btn-xs btn-info' data-original-title='Edit' onclick="javascript:window.location='<?php echo base_url().'Bommanage/manage/'.$bom_detail['bom_id'];?>';"><i class='fa fa-pencil'></i></button>

														<button type="button" class='btn btn-xs btn-danger' data-original-title='Delete' onclick="javascript:if(confirm('คุณต้องการลบรายการนี้ใช่หรือไม่?')){ window.location='<?php echo base_url().'Bommanage/delete_bom/'.$bom_detail['bom_id'];?>'; }else{ return false; }"><i class='fa fa-trash-o'></i></button> 
														</td>
												</tr>
												<?php $i++; } }else{ ?>
								                    <tr>
								                      	<td colspan="7" style="text-align:center;">Data Not Found.</td>
								                    </tr>
								                    <?php } ?>
													
											</tbody>

										</table>
										


										<?php echo form_close();?>
									</div>
									<!-- end widget content -->


						</div>
						<p>&nbsp;</p>
									
						<!-- End widget div-->
					</div>

				</div>







				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						
					<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false">

						

						<!-- widget div-->
						<div>
							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->
								
							</div>
							<!-- end widget edit box -->

							<!-- widget content -->
							<div class="widget-body no-padding">

							<?php if($frmEdit==FALSE){ ?>
								<?php echo form_open('Bommanage/add_unit/', array('id'=>'smart-form-register', 'class'=>'form-horizontal'));?>

								<div class="row">
									<div class="col-xs-12">
										<!-- <div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div> -->
										<div class="table-header">
											Details
										</div>	
										<div class="space-4"></div>
										<div class="space-4"></div>
										<div class="form-group">
												<!-- <label class="col-sm-2 control-label no-padding-right" for="form-field-2">  </label> -->

												<div class="col-xs-2 col-sm-2">

													Comp Code
														
												</div>
												<div class="col-xs-7 col-sm-7">
													<span class="block input-icon input-icon-right">
																		<input type="text" id="item_cd" name="item_cd" placeholder="Comp No." value="" class="width-100" />		
													</span><div class="space-4"></div>				
												</div>

												<div class="col-xs-1 col-sm-1">
														<span class="block input-icon input-icon-right">
																		<input type="button" class="btn btn-sm btn-default" id="search_item" name="search_item" onclick="searchExp()" value=" Check">
																			<!-- <i class="ace-icon fa fa-search bigger-110"></i> -->
																			<!-- <span class="bigger-110 no-text-shadow">Search</span>
																		</button> -->
																	</span>
												</div>
			
		
										</div>
										<input type="hidden" id="parent_cd" name="parent_cd" placeholder="Parent No." value="" class="width-100" />

<!-- 										<div class="form-group">
											
												<div class="col-xs-2 col-sm-2">

													Parent Code
														
												</div>
												<div class="col-xs-7 col-sm-7">
													<span class="block input-icon input-icon-right">
																				
													</span><div class="space-4"></div>				
												</div>

												<div class="col-xs-1 col-sm-1">
													
												</div>
			
		
										</div> -->











										<div class="form-group">
												<label class="col-sm-2 control-label no-padding-right" for="form-field-2">  </label>
												<div class="col-xs-10 col-sm-8">
													<div class="radio">
													Status
														<label>
															<input checked="checked" type="radio" name="rad_status" value="1" <?php echo set_radio('rad_status', '1', TRUE); ?> class="ace" />
															<span class="lbl"> Enable </span>

															
														</label>

														<label>
															<input type="radio" name="rad_status" value="0" <?php echo set_radio('rad_status', '0'); ?> class="ace" />
															<span class="lbl"> Disable </span>

														</label>
													</div>

												
												</div>
											</div>


									</div>
								</div>

									<div class="clearfix form-actions">
											<div class="col-md-offset-5 col-md-9">

												<button type="submit" class="btn btn-primary">
												ADD
											</button>
											<input type="hidden" name="action" value="<?php echo base64_encode('addUnit');?>"  />

												<!-- &nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button> -->
											</div>
									</div>

								<?php echo form_close(); ?>

								<?php }else{// Form Edit ?>

								<?php echo form_open('Bommanage/edit_bom/', array('id'=>'smart-form-register', 'class'=>'form-horizontal'));?>

								<input type="hidden" id="temp_id" name="temp_id" value="<?php echo $bomDataEdit['bom_id']; ?>" class="width-100" />
								<input type="hidden" id="temp_boxcd" name="temp_boxcd"  value="<?php echo $bomDataEdit['comp_code']; ?>" class="width-100" />



								<div class="row">
									<div class="col-xs-12">
										
										<div class="table-header">
											Details
										</div>	
										<div class="space-4"></div>
										<div class="space-4"></div>
										<div class="form-group">
												<!-- <label class="col-sm-2 control-label no-padding-right" for="form-field-2">  </label> -->

												<div class="col-xs-2 col-sm-2">

													Part No.
														
												</div>
												<div class="col-xs-7 col-sm-7">
													<span class="block input-icon input-icon-right">
																		<input type="text" id="item_cd" name="item_cd" placeholder="Part No." value="<?php echo $bomDataEdit['comp_code']; ?>" class="width-100" />		
													</span><div class="space-4"></div>				
												</div>

												<div class="col-xs-1 col-sm-1">
														<span class="block input-icon input-icon-right">
																		<input type="button" class="btn btn-sm btn-default" id="search_item" name="search_item" onclick="searchExp()" value=" Check">
																			<!-- <i class="ace-icon fa fa-search bigger-110"></i> -->
																			<!-- <span class="bigger-110 no-text-shadow">Search</span>
																		</button> -->
																	</span>
												</div>
			
		
										</div>



										<div class="form-group">
												<label class="col-sm-2 control-label no-padding-right" for="form-field-2">  </label>
												<div class="col-xs-10 col-sm-8">
													<div class="radio">
													Status
														<label>
															<input checked="checked" type="radio" name="rad_status" value="1" <?php echo set_radio('rad_status', '1', TRUE); ?> class="ace" />
															<span class="lbl"> Enable </span>

															
														</label>

														<label>
															<input type="radio" name="rad_status" value="0" <?php echo set_radio('rad_status', '0'); ?> class="ace" />
															<span class="lbl"> Disable </span>

														</label>
													</div>

												
												</div>
											</div>


									</div>
								</div>

									<div class="clearfix form-actions">
											<div class="col-md-offset-5 col-md-9">

												<button type="submit" class="btn btn-primary">
												Edit
											</button>
											<input type="hidden" name="action" value="<?php echo base64_encode('editBom');?>"  />

												&nbsp; &nbsp; &nbsp;
												<button class="btn" type="reset"  onclick="msg()">
													<i class="ace-icon fa fa-undo bigger-110"></i>
													Reset
												</button>
											</div>
									</div>


								<?php echo form_close(); ?>
							<?php } ?>
							

							
							</div>
							<!-- end widget content -->
						</div>
						<!-- end widget div -->


					</div>

				</div>


								
</div>






								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
<!-- ////////////////////////////////////////// END CONTENT ////////////////////////////// -->

<script type="text/javascript">
/*	 function msg() {
	   alert("Hello world!");
	 }*/


	 function msg() {
	 	/*alert ("hello")*/
		var tempboxcd = document.getElementById("temp_boxcd").value;

		window.location='<?php echo base_url().'Bommanage/manage/';?>'+ tempboxcd;
	}

	function searchExp() {
		// alert("Hello worlddd!");
				document.getElementById("item_cd").disabled = true;
				var txt_select = document.getElementById("item_cd").value;
 //alert(txt_select);

				document.getElementById("table23").innerHTML = "";

				if (txt_select == '') {
					txt_select = 'PCSYSTEM_TEAM';	
				}


				 //alert( txt_select);

				  $.ajax({
						type:"POST",
						url: 'http://192.168.161.102/exp_api3party/Test/BOM/',
						data: {data:txt_select},
						success:function(data) {
							console.log(data);

							if (data.length==1) {
								document.getElementById("item_cd").disabled = false;
								document.getElementById("item_cd").value = data[0].ITEM_CD;
							}else{
								var trHTML = '';
								var x = 1;

								for (i = 0; i < data.length; i++) {

									if (i == 0) {
										var str = 'checked="checked"';
										var tbdy = '<tr><th style="text-align:center;" width="8%">No.</th><th style="text-align:center;" width="12%">Select</th><th style="text-align:center;" width="25%">Comp Code</th><th style="text-align:center;" width="25%">Parent Code</th><th style="text-align:center;" width="5%">Denominator</th><th style="text-align:center;" width="5%">Numerator</th></tr>';

									}else{
										var str = '';
										var tbdy = '';
									}
								    

								        trHTML += tbdy +
								        '<tr><td style="text-align:center;">' + x +
						                '</td><td style="text-align:center;"><input '+str+' type="radio" name="rad_item" id="rad_item" value="'+ data[i].COMP_ITEM_CD +'"  class="ace" /><span class="lbl"></span>' 	   + 
						                '<input type="hidden" name="parent_item" id="parent_item" value="'+ data[i].PARENT_ITEM_CD +'"  class="ace" /><span class="lbl"></span>' 
						                            +
						                '</td><td>' + data[i].COMP_ITEM_CD +
						                '</td><td>' + data[i].PARENT_ITEM_CD +   
						                '</td><td>' + data[i].PS_UNIT_DENOMINATOR + 
						                '</td><td>' + data[i].PS_UNIT_NUMERATOR + 
						                '</td></tr>'; 

						                x = x+1;
								}


								$('#table23').append(trHTML);
								$('#sel_item_exp').modal('show');
							}


						},
						error: function(data){
							alert('Ajax failed');
						}
					});
			}

			function selItemexp() {
				document.getElementById("item_cd").disabled = false;
				var radios = document.getElementsByName('rad_item');
				
				var parent_code = document.getElementsByName('parent_item');
				
				for (var i = 0, length = radios.length; i < length; i++)
				{
				 if (radios[i].checked)
				 {
				  // do whatever you want with the checked radio
				  // alert(radios[i].value);
				  document.getElementById("item_cd").value = radios[i].value;
				  document.getElementById("parent_cd").value = parent_code[i].value;

				  //alert(radios[i].value);
				  // only one radio can be logically checked, don't check the rest
				  break;
				 }
				}

				// alert(radios.length);
			}
			
			function clearselItemexp() {
				document.getElementById("item_cd").disabled = false;

			}


			

			jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')

				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {

					/*bAutoWidth: false,
					 "aoColumns": [
					   { "bSortable": false },
					   null, null,null, null, null,
					   { "bSortable": false }
					 ],
					"aaSorting": [],*/
					
					"bStateSave": true,
					"StateSave": true,
        
					//"bProcessing": true,
			        //"bServerSide": true,
			        //"sAjaxSource": "http://127.0.0.1/table.php"	,
			
					//,
					//"sScrollY": "200px",
					//"bPaginate": false,
			
					//"sScrollX": "100%",
					//"sScrollXInner": "120%",
					//"bScrollCollapse": true,
					//Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
					//you may want to wrap the table inside a "div.dataTables_borderWrap" element
			
					//"iDisplayLength": 50
			
			
					select: {
						style: 'multi'
					}
			    } );



				
				
				
				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable, {
					buttons: [
					 /* {
						"extend": "colvis",
						"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
						"className": "btn btn-white btn-primary btn-bold",
						columns: ':not(:first):not(:last)'
					  },
					  {
						"extend": "copy",
						"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },*/
					  {
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					 /* {
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },*/
					 /* {
						"extend": "pdf",
						"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },*/
					  /*{
						"extend": "print",
						"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
						"className": "btn btn-white btn-primary btn-bold",
						autoPrint: false,
						message: 'This print was produced using the Print button for DataTables'
					  }		*/  
					]
				} );
				myTable.buttons().container().appendTo( $('.tableTools-container') );
		
				
				
				
				
				myTable.on( 'select', function ( e, dt, type, index ) {
					if ( type === 'row' ) {
						$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', true);
					}
				} );
				myTable.on( 'deselect', function ( e, dt, type, index ) {
					if ( type === 'row' ) {
						$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
					}
				} );
			
			
			
			
				/////////////////////////////////
				//table checkboxes
				$('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
				
				//select/deselect all rows according to table header checkbox
				$('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function(){
					var th_checked = this.checked;//checkbox inside "TH" table header
					
					$('#dynamic-table').find('tbody > tr').each(function(){
						var row = this;
						if(th_checked) myTable.row(row).select();
						else  myTable.row(row).deselect();
					});
				});
				
				//select/deselect a row when the checkbox is checked/unchecked
				$('#dynamic-table').on('click', 'td input[type=checkbox]' , function(){
					var row = $(this).closest('tr').get(0);
					if(this.checked) myTable.row(row).deselect();
					else myTable.row(row).select();
				});


				$('#btn_enable').click(function(e) {
		
				if(confirm('คุณต้องการเปิดการใช้งานนี้ใช่หรือไม่')){
					
					$('#frm_usermanagement').attr('action', '<?php echo base_url().'user/checkall_enable'; ?>');
					$('#frm_usermanagement').submit();
					
				}else{
					
					return false;
				}
				
		       
				
		    });
			
			$('#btn_disable').click(function(e) {
				
				if(confirm('คุณต้องการระงับรายการนี้ใช่หรือไม่')){
				
		       		$('#frm_usermanagement').attr('action', '<?php echo base_url().'user/checkall_disable'; ?>');
					$('#frm_usermanagement').submit();
				
				}else{
				
					return false;	
				}
				
		    });
			
			$('#btn_delete').click(function(e) {
				
				if(confirm('คุณต้องการลบรายการใช้งานนี้ใช่หรือไม่')){
				
		        	$('#frm_usermanagement').attr('action', '<?php echo base_url().'user/checkall_delete'; ?>');
					$('#frm_usermanagement').submit();
				
				}else{
					
					return false;
				}
				
		    });
		
			})

</script>


