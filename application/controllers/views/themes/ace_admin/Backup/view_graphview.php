<?php 
	$setHeadContent = strtoupper(str_replace('_', ' ', $this->router->fetch_method()));
?>
<!-- //////////////////////////////////// CONTENT ////////////////////////////////////////////// -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url().'manage/home/';?>">Home</a>
							</li>

							<li class="active">QMD System</li>
							<li class="active">Graph View</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">
						<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->
					<div class="row">
						<div class="col-xs-12">
							<h3 class="header smaller lighter blue">Graph View</h3>

							<div class="form-group row">		
						<label  class="col-sm-1 col-form-label"> Line Name: </label>
						<div class="col-xs-12 col-xs-6 col-md-4 col-lg-2">
								<label id="selectlinename">
										<?php
										/*if ($g['del_flag'] == '0')*/
										$optName = array();
										$optName['0'] = '-- Please Select Linename --';
										foreach($linename->result() as $g){
											$optName[$g->line_id] = $g->line_name;
										}
										$selected = (set_value('sel_group')) ? set_value('sel_group') : '-- Please Select Linename --';
										echo form_dropdown('sel_group', $optName,  $selected, 'style="width: 500px"');
										?>
								</label>
						</div>
					</div>
					<div class="form-group row">
							<label class="col-sm-1 col-form-label"> OP Name: </label>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
									<label id="">
											<select  style="width: 250px;" name="selectpart" id = "selectopname" >
											<option value="">-- Please Select OP Name --</option>
											</select>
									</label>	
						</div>
						
							<label class="col-sm-1 col-form-label"> PartName: </label>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
									<label id="">
											<select  style="width: 250px;" id = "selectpartname">
											<option value="">-- Please Select PartName --</option>
											</select>
									</label>
						</div>
					</div>
			
					<div class="form-group row">
						
							<label class="col-sm-1 col-form-label" > ProcessName: </label>
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
								<label  id="">
										<select  style="width: 250px;" id = "selectprocessname">
										<option value="">-- Please Select ProcessName --</option>
										</select>
							</label>
						</div>
						
							<label class="col-sm-1 col-form-label"> PartNumber: </label>
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
								<label id="">
										<select  style="width: 250px;" id = "selectpartnumber">
										<option value="">-- Please Select PartNumber --</option>
										</select>
								</label><br>
						</div>
					</div>

					<div class="form-group row">	
						<label class="col-sm-1 col-form-label"> ItemNumber: </label>
						<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
							<label id="">
									<select style="width: 250px;" id = "selectitemnumber">
									<option value="">-- Please Select ItemNumber --</option>
									</select>
							</label>
						</div>			
					<label class="col-form-label" for="from">Date From</label>
						<input type="text" id="from" name="minDate" placeholder="Select Date Here" autocomplete="off">
						<label class="col-form-label" for="to">to</label>
						<input type="text" id="to" name="maxDate" placeholder="Select Date Here" autocomplete="off">
					<button text-align: center class="btn btn-info" id="submitdate"  onclick="subdate()">Submit</button>		
				</div>
				

				

					<div id="container" style="height: 400px; min-width: 600px"></div>


					<form>
					<div class="form-group row">
						<label for="USL" class="col-sm-1 col-form-label">Process Average: </label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="AVG" name="AVG" disabled>
					</div>
					
						<label for="USL" class="col-sm-1 col-form-label">Average MR:</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="AVGMR" name="AVGMR" disabled>
					</div>
					</div>

					<div class="form-group row">
						<label for="UCL" class="col-sm-1 col-form-label">UCL:</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" id="UCL" name="UCL" disabled>
						</div>
						<label class="col-sm-1 col-form-label">LCL:</label>
						<div for="LCL" class="col-sm-2">
							<input type="text" class="form-control" id="LCL" name="LCL" disabled>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-1 col-form-label">USL:</label>
						<div for="USL" class="col-sm-2">
							<input type="text" class="form-control" id="USL" name="USL" disabled>
						</div>
						<label class="col-sm-1 col-form-label">LSL:</label>
						<div for="LSL" class="col-sm-2">
							<input type="text" class="form-control" id="LSL" name="LSL" disabled>
						</div>
					</div>

					<div class="form-group row">
						<label for="CPK" class="col-sm-1 col-form-label">CPK:</label>
					<div class="col-sm-2">
						<input type="text" class="form-control" id="CPK" name="CPK" disabled>
					</div>
					</div>
					</form>
				

				
					
										
									</div>

										<!-- div.table-responsive -->
										
										<!-- div.dataTables_borderWrap -->

						
							<!-- end widget content -->
						
						<!-- end widget div -->
					</div>
				</div>
				</div>
				
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
<!-- ////////////////////////////////////////// END CONTENT ////////////////////////////// -->

<style type="text/css">
#submitdate {
    width:100px;
    height:35px;
    text-align: center; 
	line-height: 10px;
}

</style>


<script>


function removeOptions(selectElement) {
   var i, L = selectElement.options.length - 1;
   for(i = L; i >= 0; i--) {
      selectElement.remove(i);
   }
}

$("#selectlinename").change(function(event){ // reset dropdown if nothing
	var lineid = document.getElementsByName("sel_group")[0].value
	if (lineid == 0) {
		removeOptions(document.getElementById('selectprocessname')); // reset processname
		$('#selectprocessname')
					.append($("<option></option>")
					.text("-- Please Select ProcessName --"));

		removeOptions(document.getElementById('selectpartnumber')); // reset partnumber
		$('#selectpartnumber')
					.append($("<option></option>")
					.text("-- Please Select PartNumber --"));

		removeOptions(document.getElementById('selectitemnumber')); // reset itemnumber
		$('#selectitemnumber')
					.append($("<option></option>")
					.text("-- Please Select ItemNumber --"));
	}
});
$("#selectlinename").change(function(event){
    var lineid = document.getElementsByName("sel_group")[0].value
	$.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>Graphview/showop', 
		data: {lineid:lineid},
        success: function (data) {
			removeOptions(document.getElementById('selectopname'));
			$('#selectopname')
					.append($("<option></option>")
					.text("-- Please Select OP Name --"));
			$.each( data, function( key, val ) {
				$('#selectopname')
					.append($("<option></option>")
					.attr("value",JSON.parse(val.op_id))
					.text(val.op_name));
				//items.push([JSON.parse(val.op_id),val.op_name]);
				//option = '<option value="'.$row->JSON.parse(val.op_id).'">'.$row->val.op_name.'</option>';			
			});
			//alert(items);
			//$('#selectopname1').html(data);
		}
	});

});

$("#selectlinename").change(function(event){ // if linename changed
	var lineid = document.getElementsByName("sel_group")[0].value
	$.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>Graphview/showpartname', 
		data: {lineid:lineid},
        success: function (data) {
			removeOptions(document.getElementById('selectpartname'));
			//alert(JSON.stringify(data));
			$('#selectpartname')
					.append($("<option></option>")
					.text("-- Please Select PartName --"));
			$.each( data, function( key, val ) {
				
				$('#selectpartname')
					.append($("<option></option>")
					.attr("value",JSON.parse(val.name_id))
					.text(val.partname));		
			});		
		}
	});
});

$("#selectopname").change(function(event){ // if opname changed
	var e = document.getElementById("selectopname");
	var opid = e.options[e.selectedIndex].value;
	$.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>Graphview/showprocessname', 
		data: {opid:opid},
        success: function (data) {
			removeOptions(document.getElementById('selectprocessname'));
			//alert(JSON.stringify(data));
			$('#selectprocessname')
					.append($("<option></option>")
					.text("-- Please Select ProcessName --"));
			$.each( data, function( key, val ) {
				
				$('#selectprocessname')
					.append($("<option></option>")
					.attr("value",JSON.parse(val.pro_id))
					.text(val.pro_no));		
			});		
		}
	});
});

$("#selectopname").change(function(event){ // if opname changed
	var e = document.getElementById("selectopname");
	var opid = e.options[e.selectedIndex].value;
	$.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>Graphview/showprocessname', 
		data: {opid:opid},
        success: function (data) {
			removeOptions(document.getElementById('selectprocessname'));
			//alert(JSON.stringify(data));
			$('#selectprocessname')
					.append($("<option></option>")
					.text("-- Please Select ProcessName --"));
			$.each( data, function( key, val ) {
				
				$('#selectprocessname')
					.append($("<option></option>")
					.attr("value",JSON.parse(val.pro_id))
					.text(val.pro_no));		
			});		
		}
	});
});

$("#selectpartname").change(function(event){
	var e = document.getElementById("selectpartname");
	var nameid = e.options[e.selectedIndex].value;
	
	$.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>Graphview/showpartnumber', 
		data: {nameid:nameid},
        success: function (data) {
			
			removeOptions(document.getElementById('selectpartnumber'));
			//alert(JSON.stringify(data));
			$('#selectpartnumber')
					.append($("<option></option>")
					.text("-- Please Select PartNumber --"));
			$.each( data, function( key, val ) {
				$('#selectpartnumber')
					.append($("<option></option>")
					.attr("value",JSON.parse(val.no_id))
					.text(val.partno));		
			});		
		}
	});
});

$("#selectprocessname").change(function(event){ 
	var e = document.getElementById("selectprocessname");
	var proid = e.options[e.selectedIndex].value;
	$.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>Graphview/showitemnumber', 
		data: {proid:proid},
        success: function (data) {
			removeOptions(document.getElementById('selectitemnumber'));
			//alert(JSON.stringify(data));
			$('#selectitemnumber')
					.append($("<option></option>")
					.text("-- Please Select ItemNumber --"));
			$.each( data, function( key, val ) {
				
				$('#selectitemnumber')
					.append($("<option></option>")
					.attr("value",JSON.parse(val.item_id))
					.text(val.item_no));	
			});		
		}
	});
});

function subdate() {

	genstockchart();
	// var valop = document.getElementById("selectopname");
	// var valpartname = document.getElementById("selectpartname");
	// var valproname = document.getElementById("selectprocessname");
	// var valpartnum = document.getElementById("selectpartnumber");
	// var valitem = document.getElementById("selectitemnumber");

	// var lineid = document.getElementsByName("sel_group")[0].value
	// var opid = valop.options[valop.selectedIndex].value;
	// var partnameid = valpartname.options[valpartname.selectedIndex].value;
	// var pronameid = valproname.options[valproname.selectedIndex].value;
	// var partnumid = valpartnum.options[valpartnum.selectedIndex].value;
	// var itemid = valitem.options[valitem.selectedIndex].value;
	// var mindate = document.getElementById("from").value;
	// var maxdate = document.getElementById("to").value;
	
	// $.ajax({
    //     type: 'POST',
    //     url: '<?php echo base_url();?>Graphview/showqcdetails', 
	// 	data: {lineid:lineid, opid:opid, partnameid:partnameid, pronameid:pronameid, partnumid:partnumid, itemid:itemid, mindate:mindate, maxdate:maxdate},
    //     success: function (data) {
	// 		alert(data);
	// 		genstockchart();
	// 	}
    // 	});

}

function genstockchart(){
    //document.write(JSON.stringify(data));
	var valop = document.getElementById("selectopname");
	var valpartname = document.getElementById("selectpartname");
	var valproname = document.getElementById("selectprocessname");
	var valpartnum = document.getElementById("selectpartnumber");
	var valitem = document.getElementById("selectitemnumber");

	var lineid = document.getElementsByName("sel_group")[0].value
	var opid = valop.options[valop.selectedIndex].value;
	var partnameid = valpartname.options[valpartname.selectedIndex].value;
	var pronameid = valproname.options[valproname.selectedIndex].value;
	var partnumid = valpartnum.options[valpartnum.selectedIndex].value;
	var itemid = valitem.options[valitem.selectedIndex].value;
	var mindate = document.getElementById("from").value;
	var maxdate = document.getElementById("to").value;
	$.ajax({
        type: 'POST',
        url: '<?php echo base_url();?>Graphview/showqcdetails', 
		data: {lineid:lineid, opid:opid, partnameid:partnameid, pronameid:pronameid, partnumid:partnumid, itemid:itemid, mindate:mindate, maxdate:maxdate},
        success: function (data) {

			var items = [];
			var datas = [];
			//var test = [2.92,2.96,2.86,3.04,3.07,2.85,3,2.92,2.97,2.97,3.09,3.07,2.99,3.06,3.05,3.02,3.07,2.91,3.07,3.2];		
			var mrs = [];
			var mravg = 0;
			var avg = 0;
			var tmp = 0;
			var cnt = 0;
			var dimension = 0;
			var minus = 0;
			var plus = 0;
			var morethan = 0;
			var lessthan = 0;

			$.each( data, function( key, val ) {
				items.push([JSON.parse(val.date_created),JSON.parse(val.data)]);
				datas.push(JSON.parse(val.data));
				tmp = tmp + JSON.parse(val.data);
				cnt = cnt + 1;
				dimension = JSON.parse(val.txt7);
				minus = JSON.parse(val.txt11);
				plus = JSON.parse(val.txt10);
				morethan = JSON.parse(val.txt8);
				lessthan = JSON.parse(val.txt9);
			});
			for (i = 0; i < datas.length; i++) {
				mrs.push(Math.abs(datas[i+1] - datas[i])); //CPK will be Infinite if datas is equal in every arrays	because it divided by zero	
			}
			for (i = 0; i < mrs.length-1; i++) {
				mravg = mravg + mrs[i];
				//alert(mrs.length);
			}

			// Calculate From Formula
			avg = (tmp/cnt);
			var upper = dimension + plus; //USL
			var lower = dimension - minus; //LSL
			var avgmr = (mravg / (mrs.length-1));
			var d2 = 1.128;
			var d3 = (avgmr/d2);
			var avgcpk = (3*(d3));
			//var cpk = Math.min((upper-avg)/(3*avgmr), (avg-lower)/(3*avgmr));
			//alert((upper-avg)/avgcpk + " " + (avg-lower)/avgcpk);
			var cpk = 0;
			if (lessthan){
				cpk = Math.min((upper-avg)/avgcpk, (avg-lower)/avgcpk);	// 2 arms
			}else{
				cpk = (upper-avg)/avgcpk; // only 1 arm
			}

			//insert into value textbox
			var USL = $('#USL');
			var LSL = $('#LSL');
			var AVG = $('#AVG');
			var AVGMR = $('#AVGMR');
			var UCL = $('#UCL');
			var LCL = $('#LCL');
			var CPK = $('#CPK');

			AVG.val(avg.toFixed(4));
			AVGMR.val(avgmr.toFixed(4));
			USL.val(upper.toFixed(4));
			LSL.val(lower.toFixed(4));
			UCL.val(morethan.toFixed(4));
			LCL.val(lessthan.toFixed(4));
			CPK.val(cpk.toFixed(4));

	//document.write(items);
	//console.log(Math.ceil(avg));
			const timezone = new Date().getTimezoneOffset()
					Highcharts.setOptions({
					global: {
						timezoneOffset: timezone
							}
					});
				Highcharts.stockChart('container', {
					rangeSelector: {
						//selected: 2
						enabled: false
					},
					yAxis: {
						plotLines: [{
						value: upper,
						color: 'red',
						dashStyle: 'Solid',
						zIndex: 7,
						width: 2,
						label: {
							style: {
								fontSize: '15px'
							},	
							text: 'USL'
						}
					}, {
						value: morethan,
						color: 'red',
						dashStyle: 'Solid',
						width: 2,
						zIndex: 6,
						label: {
							style: {
								fontSize: '15px'
							},
							x: 50,
							text: 'UCL'
						}
					}, {
						value: lessthan,
						color: 'green',
						dashStyle: 'Solid',
						width: 2,
						zIndex: 4,
						label: {
							style: {
								fontSize: '15px'
							},		
							x: 50,
							text: 'LCL'
						}
					}, {
						value: lower,
						color: 'green',
						dashStyle: 'Solid',
						width: 2,
						zIndex: 5,
						label: {
							style: {
								fontSize: '15px'
							},
							text: 'LSL'
						}
					}],
		
					},
					title: {
						text: 'QC System'
					},
					scrollbar: {
			            barBackgroundColor: 'gray',
			            barBorderRadius: 7,
			            barBorderWidth: 0,
			            buttonBackgroundColor: 'gray',
			            buttonBorderWidth: 0,
			            buttonBorderRadius: 7,
			            trackBackgroundColor: 'none',
			            trackBorderWidth: 1,
			            trackBorderRadius: 8,
			            trackBorderColor: '#CCC'
			        },
					series: [{
						name: 'Data',
						data: items,
						
						type: 'areaspline',
						//color: '##DE7761',
						tooltip: {
							valueDecimals: 4
						},
						
						fillColor: {
					        linearGradient: {
					          x1: 0,
					          y1: 0,
					          x2: 0,
					          y2: 1
					        },
					        stops: [
					          [0, Highcharts.getOptions().colors[0]],
					          [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
					        ]
					      },
						threshold: null
					}]
				});
        }
    	});
// Set the datepicker's date format
}

$( function() {
	
	
    var dateFormat = "mm/dd/yy",
      from = $( "#from" )
        .datepicker({
		changeMonth: true,
        changeYear: true,
		dateFormat: 'yy-mm-dd'
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to" ).datepicker({
        
        changeMonth: true,
        changeYear: true,
		dateFormat: 'yy-mm-dd'
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );

</script>

<style type="text/css">

  #centerbutton{
	  position:center;
	  top:50%;
	  left:30%;
  }
  .fa-calendar {
	  font-size: 18px;
  }
  .ui-datepicker-calendar tr, 
  .ui-datepicker-calendar td, 
  .ui-datepicker-calendar td a, 
  .ui-datepicker-calendar th{font-size:inherit;}
  div.ui-datepicker{font-size:16px;width:inherit;height:inherit;}
  .ui-datepicker-title span{font-size:16px;}
  </style>
 
 
<style type="text/css">

.highcharts-figure, .highcharts-data-table table {
    min-width: 300px; 
    max-width: 1500px;
    margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #EBEBEB;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
	font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

</style>