			<div class="main-content">
				<div class="main-content-inner">
					<div class="page-content">
												<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->


								<div class="row">
									<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Manage Userpermissions</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
								</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										</div>



								<div class="col-xs-5">
										<div class="table-header">
										<i class="fa fa-edit"></i> 
										Change password 
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										

											<!-- Table All -->
											<table class="table table-striped table-bordered table-hover">
												<tr>
													<td>
												<div class="row" style="margin:0;padding:0;">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<?php if($str_validate==FALSE){ echo validation_errors(); } ?>
						<?php if ($this->session->flashdata('msgResponse') != ''){ echo $this->session->flashdata('msgResponse'); } ?>
					</div>
				</div>
				
				<!-- row -->
				<div class="row" style="margin:0;padding:0;">
					
					<div class="col-sm-12 col-md-12 col-lg-6">

						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
							<!-- widget options:
									usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
									
									data-widget-colorbutton="false"	
									data-widget-editbutton="false"
									data-widget-togglebutton="false"
									data-widget-deletebutton="false"
									data-widget-fullscreenbutton="false"
									data-widget-custombutton="false"
									data-widget-collapsed="true" 
									data-widget-sortable="false"
									
								-->


								<!-- widget div-->
								<div>

									<!-- widget edit box -->

									<!-- end widget edit box -->

									<!-- widget content -->
									<div class="widget-body no-padding">
									<?php echo form_open('user/rule/'.$uid.'');?>	

					           			<header>
											Permissions in User Group
										</header>
					           			<fieldset>
											<section>

												<div class="input-group">
													<span class="input-group-addon"><i class="icon-append fa fa-user"></i></span>
													<input type="text" class="form-control" placeholder="Permission Name" value="<?php echo $recLoadUser['username'];?>" readonly style="background-color: #d7d7d7;">
												</div>



											</section>

											<section>

												<?php
												foreach($excPerG->result() as $pg){ 


												?>

<!-- 												<label class="checkbox">
													<input name="chkRid[]" id="subscription" type="checkbox" value="<?php echo $pg->sp_id;?>" <?php if(in_array($pg->sp_id, $excRule)===true){ echo 'checked="checked"';}?> >
													<i></i><?php echo $pg->name;?><span class="lbl"></span>
												</label> -->

														<label class="checkbox">
																<input name="chkRid[]" id="subscription" type="checkbox" class="ace" value="<?php echo $pg->sp_id;?>" <?php if(in_array($pg->sp_id, $excRule)===true){ echo 'checked="checked"';}?> />
																<span class="lbl"></span>
																<i></i><?php echo $pg->name;?><span class="lbl"></span>
														</label>




												<?php } ?>

											</section>
											
										</fieldset>

<!-- 
										<header>
											Other Permissions
										</header> -->

										<fieldset>
<!-- 											<section>
												<?php
												foreach($excOthRule->result() as $pg){ 


												?>


												<label class="checkbox">
													<input name="chkRid[]"  id="subscription" type="checkbox" value="<?php echo $pg->sp_id;?>" <?php if(in_array($pg->sp_id, $excRule)===true){ echo 'checked="checked"';}?> >
													<i></i><?php echo $pg->name;?>
												</label>

												<?php } ?>

											</section> -->
										</fieldset>

										<p>&nbsp;</p>
										<footer>
											<button type="submit" class="btn btn-primary">
												Save
											</button>
											<input type="hidden" name="action" value="<?php echo base64_encode('saveRuleUser');?>"  />
										</footer>

					           		<?php echo form_close();?>
									</div>
									<!-- end widget content -->

								</div>
								<!-- end widget div -->
						</div>

					</div>

				</div>


													</td>
												</tr>
											
											</table>
											</div>
										</div>
					</div>
			</div>	