			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url().'manage/home/';?>">Home</a>
							</li>

							<li class="active">Settings</li>
							<li class="active">Edit Profile</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>
					<div class="page-content">
						<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->
									<div class="row">
									<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Accout Editprofile</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
									</div>



								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="table-header">
										<i class="fa fa-edit"></i> 
										Edit Profile
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										

											<!-- Table All -->

											
														<div class="space-4"></div>
														<div class="row" style="margin:0;padding:0;">
															<div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
																<?php if($str_validate==FALSE){ echo validation_errors(); } ?>
																<?php if ($this->session->flashdata('msgResponse') != ''){ echo $this->session->flashdata('msgResponse'); } ?>
															</div>
														</div>

														<?php echo form_open('editprofile/account', array('id'=>'smart-form-register', 'class'=>'smart-form'));?>	
														           		
					           			
					           			

											<section>
												<div class="input-group">
													
													<input name="txt_usr" type="text" class="form-control" type="email" placeholder="Username" value="<?php echo $this->session->userdata('sessUsr');?>" style="background-color: #d7d7d7;" readonly>
													<span class="input-group-addon"><i class="icon-append fa fa-user"></i></span>
												</div>
											</section>
										
									<div class="space-4"></div>
									
												<div >
													
													
													<input name="txt_fname" type="text" class="form-control" type="email" placeholder="First name" value="<?php echo $recLoadUser['firstname'];?>">
													<div class="space-4"></div>
												
													
													
													<input name="txt_lname" type="text" class="form-control" type="email" placeholder="Last name" value="<?php echo $recLoadUser['lastname'];?>">
												
												</div>


													<!-- <label class="input">
															<input type="text" name="txt_fname" placeholder="First name" value="<?php echo $recLoadUser['firstname'];?>">
													</label> -->
												
												
													<!-- <label class="input">
															<input type="text" name="txt_lname" placeholder="Last name" value="<?php echo $recLoadUser['lastname'];?>">
													</label> -->
													
											 
												
											
										
									
									
									<div class="space-4"></div>
									<div class="space-4"></div>
										
											<section>

												<label> Sex :
													<label>
														<input name="rad_sex" value="male" type="radio" class="ace" <?php if($recLoadUser['gender']=='male'){ echo 'checked="checked"';} ?> />
														<span class="lbl"> Male</span>
													<label>
														<input name="rad_sex" value="female" type="radio" class="ace" <?php if($recLoadUser['gender']=='female'){ echo 'checked="checked"';} ?> />
														<span class="lbl"> Female</span>
												</label>

											</section>
										
									<div class="space-2"></div>
										


												<div class="input-group">
													
													<input name="txt_email" type="email" class="form-control" type="email" placeholder="E-mail" value="<?php echo $recLoadUser['email'];?>">
													<span class="input-group-addon"><i class="icon-prepend fa fa-envelope-o"></i></span>
												</div>


											<!-- <section>

											<div class="input-group">
												<span class="input-group-addon"><i class="icon-prepend fa fa-envelope-o"></i></span>
												<input name="txt_email" type="text"  type="email" placeholder="E-mail" value="<?php echo $recLoadUser['email'];?>">
											</div>


											</section> -->
										
									<div class="space-4"></div>				
											
										</fieldset>
									
										
										<!-- <td style="text-align: right; vertical-align: right;"> -->
											<button type="submit" class="btn btn-primary">
												Edit Profile
											</button>
											<input type="hidden" name="action" value="<?php echo base64_encode('editprofile');?>"  />
										

									

					           		<?php echo form_close();?>
									</div>








										</div>
									</div>
								</div>
							</div>
											

												