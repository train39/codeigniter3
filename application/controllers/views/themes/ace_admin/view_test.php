<style>
.loading-page{
  background: #0d0d0d;
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
}
  .counter{
    text-align: center;
  } 
    p{
      font-size: 40px;
      font-weight: 100;
      color: #f60d54 ;
    }  
    h1 {
      color: white;
      font-size: 60px;
      margin-top: -10px;
    }
    hr{
      background: #f60d54 ;
      border: none;
      height: 1px;
    } 
  .counter{
    position: relative;
    width: 200px;
    }
    h1.abs{
      position: absolute;
      top: 0;
      width: 100%;
    }
    .color{
      width: 0px;
      overflow: hidden;
      color: #f60d54 ;
    }
.form-control {
    border-radius: 4px;
    box-shadow: none;
    border-color: #c1b8b8;
}

.btn-pink {
  background-color: #363d4a;
  border-color: #b9b9b9;
  color: #fff;
}
.btn-pink:hover {
  background-color: #63676e;
  border-color: #b9b9b9;
  color: #fff;
}

@media (min-width: 992px){
.layout-top-nav  > .navbar, .layout-top-nav .content-wrapper, .layout-top-nav .main-footer {
  max-width: 2000px;
    width: 99%;
    margin: 0 auto;
}}
@media (min-width: 992px){
#example {
  max-width: 2000px;
    width: 99%;
    margin: 0 auto;
    min-height: 56px;
}}
@media (min-width: 992px){}
.layout-top-nav .content-wrapper {
    margin-top: 126px;
}}
/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
#loading {
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   position: fixed;
   display: block;
   opacity: 0.7;
   background-color: #fff;
   z-index: 99;
   text-align: center;
}

#loading-content {
  position: absolute;
  top: 50%;
  left: 50%;
  text-align: center;
  z-index: 100;
}

.hide{
  display: none;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height: 886px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-truck"></i> TRACEABILITY REPORT
      </h1>
    </section> 
    <section class="content">
      <div class="row">
        <div class="col-12">
          <?php //echo form_open_multipart( 'Order_history/download/' ); ?>
          <!-- <form method="GET" action="http://192.168.161.102/excel_gen/Shp_his/shp_his_report?&delivery_date=%s&delivery_date2=%s" enctype="multipart/form-data"> -->
            <form method="GET" action="http://192.168.161.102/excel_gen/Traceability_his/tra_his_report?&ship_id=%s&item_cd=%s&model=%s&delivery_date=%s&delivery_date2=%s" enctype="multipart/form-data">
            <div class="box">
              <div class="box-body pb-0">
                  <div class="box-header with-border">
                    <h4 class="box-title"><i class="  fa fa-file-text"></i> Please select details to export the report.</h4>
                    <!-- <ul class="box-controls pull-right">Please select period time for export the report.</ul>                 -->
                    <ul class="box-controls pull-right">Application > Traceability History Data</ul>  
                  </div>
                  <?php //echo form_open_multipart('http://192.168.161.102/excel_gen/Del_plan/del_plan_report?year=year&month=month');?>
                  <div class="box-body">
                       <div class="row">
                        <div class="col-lg-3 col-3"> 
                          <div class="form-group">
                            <h5>Select Delivery Date ( Start ) : <span class="text-danger">*</span></h5>
                              <div class="controls">
                                      <input type="date" name="delivery_date" id="delivery_date_start" class="form-control" value="" required data-validation-required-message="This field is required."> 
                                  </div>
                              </div>
                        </div>
                        <div class="col-lg-3 col-3"> 
                          <div class="form-group">
                            <h5>Select Delivery Date ( End ) : <span class="text-danger">*</span></h5>
                              <div class="controls">
                                      <input type="date" name="delivery_date2" id="delivery_date_end" class="form-control" value="" required data-validation-required-message="This field is required."> 
                                  </div>
                              </div>
                        </div>
                    </div>

                  </div>
                  <div class="box-footer text-right">
                    <div class="row">
                      <div class="col-lg-12 col-12"> 
                        <div class="form-group">       
                          <button class="btn btn-social btn-pink pull-right" data-original-title='DOWNLOAD' type='submit'><i class="fa fa-download" ></i>E x p o r t D a t a</button>              
                                           
                      </div>
                      </div>                                   
                    </div>
                  </div>
              </div>
          </div> 
          </form>          
          <button class="btn btn-social btn-pink pull-right" id="OK" type='submit'><i class="fa fa-search" ></i>S e a r c h D a T a</button>    
        
<table id="tablaDirectorio" cellspacing="0" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
  <thead>
    <tr>
      <th class="th-sm">INVOICE_NOOO
      </th>
      <th class="th-sm">ITEM_CD
      </th>
      <th class="th-sm">ITEM_NAME
      </th>
      <th class="th-sm">MODEL
      </th>
      <th class="th-sm">SHIP_INDICATION_QTY
      </th>
      <th class="th-sm">SHIP_QTY
      </th>
      <th class="th-sm">CREATED_DATE
      </th>
    </tr>
  </thead>
  <tbody>
  </tbody>
</table>                 
</div>
</div>



</section>

<script language="JavaScript" src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script language="JavaScript" src="https://cdn.datatables.net/responsive/2.1.1/js/responsive.bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript">	
			jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#tablaDirectorio')
				.DataTable( {
					//dom: 'Bfrtip',
					bAutoWidth: false,
					"processing": true,
          "language": {
					"processing": "</span>&emsp;Loading ... </div>" 
					},
          
					"ajax": {
						"url": "http://192.168.161.102/api_system/Api_movelocation/data_server",
            "dataSrc": 'data',
						 "data": function (d) { 
						 	d.minDate = $( '#delivery_date_start' ).val();
						 	d.maxDate = $( '#delivery_date_end' ).val();
						 }
					},

          "columnDefs": [ {
                          "targets": [6],
                          "data": "CREATED_DATE"
                        } ],
					columns: [
            { data: "INVOICE_NO" },
            { data: "ITEM_CD" },
            { data: "ITEM_NAME" },
            { data: "MODEL" },
            { data: "SHIP_INDICATION_QTY" },
            { data: "SHIP_QTY" },
            //{ data: "CREATED_DATE" }
        ],
				
        select: {
          style: 'multi',
        }
        } );

			})

    $('#OK').click(function(e) {
      $('#tablaDirectorio').DataTable().ajax.reload();	
          return false;
      });
</script>