<!-- PAGE CONTENT BEGINS -->
	<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url().'manage/home/';?>">Home</a>
							</li>

							<li class="active">Groups</li>
							<li class="active">Manage User Group</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>
					<div class="page-content">

							<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->

									<div class="row">
									<div class="col-xs-12">

										<h3 class="header smaller lighter blue">Manage Usergroup</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										</div>
										<?php if ($this->session->flashdata('msgError') != ''){ echo $this->session->flashdata('msgError'); } ?>
										<?php if ($this->session->flashdata('msgResponse') != ''){ echo $this->session->flashdata('msgResponse'); } ?>
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
											

										<table id="simple-table" class="table  table-bordered table-hover">
											<tbody>
											<div class="table-header">
											<span class="widget-icon"> <i class="fa fa-table"></i> </span>List Usergroup
											</div>
											
											</tbody>
											
												<tr>
	
													<th class="detail-col">No.</th>
													<th>Name</th>
													<th>Status</th>
													<th class="hidden-480">Action</th>


												</tr>
											


												<?php
												 

												  $list_usergroup = array_filter($list_usergroup);

										    	  if (!empty($list_usergroup)) {
												  	 
												   
												  $i = 1;	  											  
												  foreach ($list_usergroup as $usergroup_detail){
												  	if ($usergroup_detail['del_flag'] == '0'){

													  if ($usergroup_detail['enable'] == '1'){ 
													  
													  	$txt_status = '<span class="label label-sm label-success">Enable</span>'; 
													  	$txt_color = '#0EC952';
													  
													  }else{
														 
														$txt_status = '<span class="label label-sm label-warning">Disable</span>';
													  	$txt_color = '#FF0000'; 
														
													  }
											  
													 
												?>


												<tr>
													<td style="text-align:center;"><?php echo $i;?></td>
													<td><?php echo $usergroup_detail['name'];?></td>
													
													<td><?php echo '<span style="color:'.$txt_color.'">'.$txt_status.'</span>';?></td>
													
													<td>

													<button type="button" class='btn btn-xs btn-info' data-original-title='Edit' onclick="javascript:window.location='<?php echo base_url().'usergroup/manage/'.$usergroup_detail['sug_id'];?>';"><i class='fa fa-pencil'></i></button>

													<button type="button" class='btn btn-xs btn-danger' data-original-title='Delete' onclick="javascript:if(confirm('คุณต้องการลบรายการนี้ใช่หรือไม่?')){ window.location='<?php echo base_url().'usergroup/delete/'.$usergroup_detail['sug_id'];?>'; }else{ return false; }"><i class='fa fa-trash-o'></i></button>
													</td>

												</tr>
												
												<?php $i++; } }}else{ ?>
							                    <tr>
							                      	<td colspan="4" style="text-align:center;">Data Not Found.</td>
							                    </tr>
							                    <?php } ?>

										</table>
									</div>

									
									<!-- /.span -->
								<!-- /.row -->


								




								
									
										
										<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">

										<div class="table-header">
											<span class="widget-icon"> <i class="fa fa-group"></i> </span>Form Usergroup
											</div>
											<?php if($frmEdit==FALSE){ ?>
											<?php echo form_open('usergroup/add', array('id'=>'smart-form-register', 'class'=>'smart-form'));?>
											
										<fieldset>
										
										<div class="space-4"></div>
											

										<div class="input-group">
										 <span class="input-group-addon"><i class="icon-append fa fa-group"></i></span>

										 <input name="txt_name" type="text" class="form-control"  placeholder="Usergroup Name">

										</div>


									


											<div class="space-4"></div>
											
												<label >Status :


												<div class="radio">
													<label>
														<input checked="checked" name="rad_status" value="1" type="radio" class="ace" <?php echo set_radio('rad_status', '1', TRUE); ?> />
														<span class="lbl"> Enable</span>
													</label>
												

												
													<label>
														<input name="rad_status" value="0" type="radio" class="ace" <?php echo set_radio('rad_status','0'); ?> />
														<span class="lbl"> Disable</span>
													</label>
												</div>


														

														<!-- <label><input checked="checked" type="radio" name="rad_status" value="1" <?php echo set_radio('rad_status', '1', TRUE); ?> />
														<i></i>Enable</label>
													
														 <label><input type="radio" name="rad_status" value="0" <?php echo set_radio('rad_status','0'); ?> />
														<i></i>Disable</label> -->

														

												</label>
											
									</fieldset>
									<footer>
										<button type="submit" class="btn btn-primary">
											Add
										</button>
										<input type="hidden" name="action" value="<?php echo base64_encode('addUsergroup');?>"  />
									</footer>
									
								<?php echo form_close(); ?>

							<?php }else{// Form Edit ?>

								<?php echo form_open('usergroup/edit/'.$usrDataEdit['sug_id'].'', array('id'=>'smart-form-register', 'class'=>'smart-form'));?>
									
							<div class="space-4"></div>
									<fieldset>

										<div class="input-group">

										 <span class="input-group-addon"><i class="icon-append fa fa-group"></i></span>

										 <input name="txt_name" type="text"  class="form-control" placeholder="Usergroup Name" value="<?php echo $usrDataEdit['name']; ?>">
										 
										</div>

										<div class="space-4"></div>

											
												<label >Status : </label>
												

													<div class="radio">
													<label>
														<input checked="checked" name="rad_status" value="1" type="radio" class="ace" <?php echo set_radio('rad_status', '1', TRUE); ?> />
														<span class="lbl"> Enable</span>
													</label>
												

												
													<label>
														<input name="rad_status" value="0" type="radio" class="ace" <?php echo set_radio('rad_status','0'); ?> />
														<span class="lbl"> Disable</span>
													</label>
												</div>
											
											
												

												<?php
												foreach($excPerG->result() as $pg){ 

												?>
<!-- 												<label class="">
													<input name="chkRid[]" id="subscription" type="checkbox" value="<?php echo $pg->spg_id;?>" <?php if(in_array($pg->spg_id, $excRule)===true){ echo 'checked="checked"';}?> >
													<i> </i><?php echo $pg->name;?>
												</label> -->

														<label class="checkbox">
																<input name="chkRid[]" id="subscription" type="checkbox" class="ace" value="<?php echo $pg->spg_id;?>" <?php if(in_array($pg->spg_id, $excRule)===true){ echo 'checked="checked"';}?> />
																<span class="lbl"></span>
																<i></i><?php echo $pg->name;?><span class="lbl"></span>
														</label>




												<?php } ?>


											
									</fieldset>

									<footer>
										<button type="submit" class="btn btn-primary">
											Save
										</button>
										<button type="submit" name="btn_saveapply" class="btn btn-primary" value="T">
											Save &amp; Apply for all users
										</button>
										<input type="hidden" name="action" value="<?php echo base64_encode('editUsergroup');?>"  />
									</footer>
								<?php echo form_close(); ?>
							<?php } ?>
									
							
								</table>
								</div>
							</div>
						</div>
					</div>





								<!-- PAGE CONTENT ENDS -->

					<!-- inline scripts related to this page -->
		