<?php 
	$setHeadContent = strtoupper(str_replace('_', ' ', $this->router->fetch_method()));
?>
<!-- //////////////////////////////////// CONTENT ////////////////////////////////////////////// -->
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url().'manage/home/';?>">Home</a>
							</li>

							<li class="active">QMD System</li>
							<li class="active">Data View</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">
						<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->

						
							<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Manage User</h3>
										
										</div>
						

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
								<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
							<!-- widget options:
									usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
									
									data-widget-colorbutton="false"	
									data-widget-editbutton="false"
									data-widget-togglebutton="false"
									data-widget-deletebutton="false"
									data-widget-fullscreenbutton="false"
									data-widget-custombutton="false"
									data-widget-collapsed="true" 
									data-widget-sortable="false"
									
								-->


								<div class="row" style="margin: 0px;padding: 0px;">
									<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
										
									</div>
									<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
										
									</div>
								</div>

								

								<!-- widget div-->
								<div>

									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
										
									</div>
									<!-- end widget edit box -->

									<!-- widget content -->
									<div class="widget-body no-padding">
									
									<?php if ($this->session->flashdata('msgError') != ''){ echo $this->session->flashdata('msgError'); } ?>
									<?php if ($this->session->flashdata('msgResponse') != ''){ echo $this->session->flashdata('msgResponse'); } ?>
					                <?php echo form_open('Dataview/data_server' , array('id' => 'frm_usermanagement', 'name'=>'frm_usermanagement', 'class'=>'form-horizontal'));?>
										
					           		<div class="row">
									   <div class="col-xs-12 col-sm-5 col-md-5 col-lg-12">
									   

										
										<div class="table-header">
											List Users
										</div>	
										
										<table id="dynamic-table" class="table table-striped table-bordered table-hover">
												
												<thead>
													
													<tr>
								                    	<td colspan="15">
														<div id="btn_checkall_data" class="btn btn-sm btn-success"><span class="fa fa-check-square-o"></span></div>
														<label style ="margin-left:1em" for="from">From</label>
														<input type="text" id="from" name="minDate" value="<?php echo $mindate; ?>" placeholder="Select Date Here" autocomplete="off">
														<label for="to">to</label>
														<input type="text" id="to" name="maxDate" value="<?php echo $maxdate; ?>" placeholder="Select Date Here" autocomplete="off">		
														<button  class="btn btn-primary"  id ="submitdate" type="submit">Submit</button>
														<input type="hidden" name="action"   value="<?php echo base64_encode('SubmitDate');?>" />
        
															
											<div class="pull-right tableTools-container">
										
								                    	</td>
								                    </tr>	
													<tr>
													<th class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace"  />
																<span class="lbl"></span>
															</label>
														</th>
														<th style="text-align:center;">
															No.
														</th>
														<th>Line Name</th>
														<th>OP Name</th>
														<th>Part Name</th>
														<th>Part Number</th>
														<th>Process Number</th>
														<th>Item Number</th>
														<th>Shift</th>
														<th>Time</th>
														<th>Data</th>
														<th>
															<i class="ace-icon fa fa-calendar bigger-110 hidden-480"></i>
															Date Created
														</th>
														<th>Action</th>
														<th>Checked By Employee</th>
														<th>Checked By Leader</th>

													</tr>
												</thead>
												<tbody>
												
											</tbody>

										</table>
									 <?php echo form_close();?> 
									</div>
									<!-- end widget content -->

								</div>
								<!-- end widget div -->
								<div id="modaledit" class="modal fade"  role="dialog">
								  <div class="modal-dialog">

								    <!-- Modal content-->
								    <div class="modal-content">
								      <div class="modal-dialog" >
										<div class="modal-content">
											<div class="modal-header no-padding">
												<div class="table-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
														<span class="white">&times;</span>
													</button>
													Edit Data
												</div>
											</div>
											
											<div class="modal-body no-padding" style="height: 400px; overflow: auto;">
												<!-- <div style="height: 400px; overflow: auto;"> --><p>
												<?php echo form_open('Dataview/edit/');?>
												&nbsp; &nbsp;<label for="data">Data :
													<input type="text" id="data" name="data" >
													<input type="hidden" id="qcid" name="qcid" >

													<input type="hidden" id="from" name="minDate" value="<?php echo $mindate; ?>"  autocomplete="off">		
													<input type="hidden" id="to" name="maxDate" value="<?php echo $maxdate; ?>"  autocomplete="off">
													</label>		
												<!-- </div> -->
											</div>
											
											<div class="modal-footer no-margin-top">
												<button class="btn btn-sm btn-danger " data-dismiss="modal" name="back3" id="back3">
													<i class="ace-icon fa fa-undo"></i>
													Cancel
												</button>

												&nbsp; &nbsp; &nbsp;

												<button class="btn btn-sm btn-success pull-right" type="submit" name="selitem" id="selitem" >
													<i class="ace-icon glyphicon glyphicon-ok"></i>
													Submit &nbsp;&nbsp;
												</button>
												

												
											</div>
										</div><!-- /.modal-content -->
										<?php echo form_close();?>
									</div><!-- /.modal-dialog -->
								</div>
								</div>
								</div>

								
						</div>

					</div>

				</div>
			
								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->
<!-- ////////////////////////////////////////// END CONTENT ////////////////////////////// -->

<style type="text/css">
#submitdate {
    width:100px;
    height:35px;
    text-align: center; 
	line-height: 10px;
}

</style>

 

<script type="text/javascript">
	

		
			jQuery(function($) {
				
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					//dom: 'Bfrtip',
					bAutoWidth: false,
					//"processing": true,
					"serverSide": true,
					"ajax": "<?php echo base_url();?>dataview/data_server",
					"aoColumnDefs": [
						{ 'bSortable': false, 'aTargets': [0] },
						{
							"targets": 0,	
							render: function ( data, type, row ) {
								if ( type === 'display' ) {
									return '<label class="pos-rel"><input type="checkbox" class="ace" name="chk_uid[]" value="' + $('<div/>').text(data).html() + '"><span class="lbl"></span></label>';
								}
								return data;
							},

						},

						// {
						// 	"targets": [11],
						// 	"type" : "date",
						// 	"render": function (data) {
						// 		if (data !== null) {
						// 			var javascriptDate = new Date(data);
						// 			javascriptDate = javascriptDate.getMonth() + 1 + "/" + javascriptDate.getDate() + "/" + javascriptDate.getFullYear();
						// 			return javascriptDate;
						// 		} else {
						// 			return "";
						// 		}
						// 	}
						// },

						{
							"targets": 12,
							render: function (data, type, row) {
								return '<span style="display:none">' + data + '</span><button class="btn btn-xs btn-info" "><i class="fa fa-pencil"></i></button>'
							+ " " + "<button class='btn btn-xs btn-danger'><i class='ace-icon fa fa-trash-o'></i></button>"
							}
						},
					

						{
							"targets": 14,	
							render: function ( data, type, row ) {
								
								if ( type === 'display' ) {
										
										return data;
								}
								return data;
							},

						},
                    ],
				
					select: {
						style: 'multi',
					}
			    } );
				

				function isNumeric(str) {
				if (typeof str != "string") return false // we only process strings!  
				return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
						!isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
				}

				$('#dynamic-table').on('click', '.btn-info', function (e) {
					var strqcid = $(this).closest('tr').find('td:eq(12)').text();
					var parseqcid = parseInt(strqcid);

					var strdata = $(this).closest('tr').find('td:eq(10)').text();
					//var fdata = isNumeric(data);
					if (isNumeric(strdata)){
						var parsedata = parseFloat(strdata);
					}else{
						var parsedata = strdata;
					}
					//alert (fdata);

					// data --------
					var data = $('#data');
					data.val(parsedata);
					//qcid --------
					var qcid = $('#qcid');
					qcid.val(parseqcid);

					$('#modaledit').modal('show');
					return false;
				});

				$('#dynamic-table').on('click', '.btn-danger', function (e) {
					var val = $(this).closest('tr').find('td:eq(12)').text(); 

					if(confirm('คุณต้องการลบรายการนี้ใช่หรือไม่?')){
					$('#frm_usermanagement').attr('action', '<?php echo base_url().'Dataview/delete_qc_detail/';?>'+ val);
					$('#frm_usermanagement').submit();
					}else{
						return false;
					}
				});


				$.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';
				
				new $.fn.dataTable.Buttons( myTable, {
					buttons: [
					//   {
					// 	"extend": "colvis",
					// 	"text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
					// 	"className": "btn btn-white btn-primary btn-bold",
					// 	columns: ':not(:first):not(:last)'
					//   },
					//   {
					// 	"extend": "copy",
					// 	"text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
					// 	"className": "btn btn-white btn-primary btn-bold"
					//   },
					  {
						"extend": "csv",
						"text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					  {
						"extend": "excel",
						"text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
						"className": "btn btn-white btn-primary btn-bold"
					  },
					//   {
					// 	"extend": "pdf",
					// 	"text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
					// 	"className": "btn btn-white btn-primary btn-bold"
					//   },
					//   {
					// 	"extend": "print",
					// 	"text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
					// 	"className": "btn btn-white btn-primary btn-bold",
					// 	autoPrint: false,
					// 	message: 'This print was produced using the Print button for DataTables'
					//   }		  
					]
				} );



				myTable.buttons().container().appendTo( $('.tableTools-container') );
		

				myTable.on( 'select', function ( e, dt, type, index ) {
					if ( type === 'row' ) {
						$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', true);
					}
				} );
				myTable.on( 'deselect', function ( e, dt, type, index ) {
					if ( type === 'row' ) {
						$( myTable.row( index ).node() ).find('input:checkbox').prop('checked', false);
					}
				} );
			
			
			
				/////////////////////////////////
				//table checkboxes
				$('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);
				
				//select/deselect all rows according to table header checkbox
				$('#dynamic-table > thead > tr > th input[type=checkbox], #dynamic-table_wrapper input[type=checkbox]').eq(0).on('click', function(){
					var th_checked = this.checked;//checkbox inside "TH" table header
					
					$('#dynamic-table').find('tbody > tr').each(function(){
						var row = this;
						if(th_checked) myTable.row(row).select();
						else  myTable.row(row).deselect();
					});
				});
				
				//select/deselect a row when the checkbox is checked/unchecked
				$('#dynamic-table').on('click', 'td input[type=checkbox]' , function(){
					var row = $(this).closest('tr').get(0);
					if(this.checked) myTable.row(row).deselect();
					else myTable.row(row).select();
				});


				$('#btn_checkall_data').click(function(e) {
					
				if(confirm('คุณต้องการยืนยันการตรวจสอบข้อมูลใช้หรือไม่')){
					
					$('#frm_usermanagement').attr('action', '<?php echo base_url().'Dataview/checkall_data';?>');
					$('#frm_usermanagement').submit();
					
				}else{
					
					return false;
				}
				
		       
				
		    });
			
			$('#btn_disable').click(function(e) {
				
				if(confirm('คุณต้องการระงับรายการนี้ใช่หรือไม่')){
				
		       		$('#frm_usermanagement').attr('action', '<?php echo base_url().'user/checkall_disable'; ?>');
					$('#frm_usermanagement').submit();
				
				}else{
				
					return false;	
				}
				
		    });
			
			$('#btn_delete').click(function(e) {
				
				if(confirm('คุณต้องการลบรายการใช้งานนี้ใช่หรือไม่')){
				
		        	$('#frm_usermanagement').attr('action', '<?php echo base_url().'user/checkall_delete'; ?>');
					$('#frm_usermanagement').submit();
				
				}else{
					
					return false;
				}
				
		    });
			})

// test below

// Date range filter

		</script>
		
<style type="text/css">

#centerbutton{
	position:center;
	top:50%;
	left:30%;
}
.fa-calendar {
	font-size: 18px;
}
.ui-datepicker-calendar tr, 
.ui-datepicker-calendar td, 
.ui-datepicker-calendar td a, 
.ui-datepicker-calendar th{font-size:inherit;}
div.ui-datepicker{font-size:16px;width:inherit;height:inherit;}
.ui-datepicker-title span{font-size:16px;}
</style>

<script type="text/javascript">
		$('#submitdate').click(function(e) {
			alert("hello na");
				
			return false;
	});
</script>


