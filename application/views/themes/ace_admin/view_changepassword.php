			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="<?php echo base_url().'manage/home/';?>">Home</a>
							</li>

							<li class="active">Settings</li>
							<li class="active">Change Password</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>
					<div class="page-content">
												<div class="ace-settings-container" id="ace-settings-container">
							<div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
								<i class="ace-icon fa fa-cog bigger-130"></i>
							</div>

							<div class="ace-settings-box clearfix" id="ace-settings-box">
								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<div class="pull-left">
											<select id="skin-colorpicker" class="hide">
												<option data-skin="no-skin" value="#438EB9">#438EB9</option>
												<option data-skin="skin-1" value="#222A2D">#222A2D</option>
												<option data-skin="skin-2" value="#C6487E">#C6487E</option>
												<option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
											</select>
										</div>
										<span>&nbsp; Choose Skin</span>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
										<label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
										<label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
										<label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
										<label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
										<label class="lbl" for="ace-settings-add-container">
											Inside
											<b>.container</b>
										</label>
									</div>
								</div><!-- /.pull-left -->

								<div class="pull-left width-50">
									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
										<label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
										<label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
									</div>

									<div class="ace-settings-item">
										<input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
										<label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
									</div>
								</div><!-- /.pull-left -->
							</div><!-- /.ace-settings-box -->
						</div><!-- /.ace-settings-container -->
								<div class="row">
									<div class="col-xs-12">
										<h3 class="header smaller lighter blue">Account Changepassword</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
								</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
										<div class="table-header">
										<i class="fa fa-edit"></i> 
										Change password 
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										

											<!-- Table All -->
											
												<div class="row" style="margin:0;padding:0;">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<?php if($str_validate==FALSE){ echo validation_errors(); } ?>
						<?php if ($this->session->flashdata('msgResponse') != ''){ echo $this->session->flashdata('msgResponse'); } ?>
					</div>
				</div>
				
				<!-- row -->
				<div class="row" style="margin:0;padding:0;">
					
					

						<!-- Widget ID (each widget will need unique ID)-->
						<div class="jarviswidget" id="wid-id-4" data-widget-editbutton="false" data-widget-custombutton="false">
							<!-- widget options:
									usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
									
									data-widget-colorbutton="false"	
									data-widget-editbutton="false"
									data-widget-togglebutton="false"
									data-widget-deletebutton="false"
									data-widget-fullscreenbutton="false"
									data-widget-custombutton="false"
									data-widget-collapsed="true" 
									data-widget-sortable="false"
									
								-->


								<!-- widget div-->
								<div>

									<!-- widget edit box -->

									<!-- end widget edit box -->

									<!-- widget content -->
									<div class="widget-body no-padding">
									
					           		<?php echo form_open('changepassword/account');?>
					           		<div class="space-4"></div>	

											<section >
												<div class="input-group">
													
													<input name="txt_usr" type="text" class="form-control" placeholder="Username" value="<?php echo $this->session->userdata('sessUsr');?>" style="background-color: #d7d7d7;" readonly>
													<span class="input-group-addon"><i class="icon-append fa fa-user"></i></span>
												</div>

											</section>
											<div class="space-4"></div>

											<section>

												<div class="input-group">
													<input name="txt_oldpwd" type="password" class="form-control" placeholder="Old Password" id="password" value="<?php echo set_value('txt_oldpwd'); ?>" >
													<span class="input-group-addon"><i class="icon-append fa fa-lock"></i></span>
												</div>
											</section>
											<div class="space-4"></div>

											<section>
												<!-- <label class="input"> <i class="icon-append fa fa-lock"></i>
													<input type="password" size="30" name="txt_newpwd" placeholder="New Password" id="password" value="<?php echo set_value('txt_newpwd'); ?>">
													<b class="tooltip tooltip-bottom-right">Don't forget your new password</b> </label> -->

												<div class="input-group">
													<input name="txt_newpwd" type="password" class="form-control" type="email" placeholder="New Password" id="password" value="<?php echo set_value('txt_newpwd'); ?>" >
													<span class="input-group-addon"><i class="icon-append fa fa-lock"></i></span>
												</div>
											</section>

											<div class="space-4"></div>

											<section>
													<div class="input-group">
													<input name="txt_cfpwd" type="password" class="form-control" type="email" placeholder="Confirm Password" id="password" value="<?php echo set_value('txt_cfpwd'); ?>" >
													<span class="input-group-addon"><i class="icon-append fa fa-lock"></i></span>
												</div>

											</section>
											
											
										<div class="space-4"></div>	
											
										

										<footer>
											<button type="submit" class="btn btn-primary">
												Change Password
											</button>
											<input type="hidden" name="action" value="<?php echo base64_encode('changepassword');?>"  />
										</footer>

					           		<?php echo form_close();?>
									</div>
									<!-- end widget content -->

								</div>
								<!-- end widget div -->
						</div>

					</div>

				</div>								
					</div>
					</div>
					</div>
				