		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
								<div class="animate__bounceIn">
								<img class="nav-user-photo" src="<?php echo base_url(); ?>./themes/ace_admin/img/analyze.png" alt="Box icon" height="150" width="150"/>
									<!-- <i class="ace-icon fa fa-leaf green" height="150" width="150" ></i> -->
								</div><br>
								<div class="animate__animated animate__fadeIn">
									<span class="Black" style="font-size:26px" id="id-text2">Quality Measurement Data Manage System</span>
									</div>
								</h1><div class="animate__animated animate__fadeIn">
								<h4 class="blue" id="id-company-text">&copy; TBKK Company</h4>
								</div>
							</div>
							
							<div class="animate__animated animate__fadeIn">
							<div class="space-6"></div>
						
							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												Sign In
												<img class="" src="<?php echo base_url(); ?>./themes/ace_admin/img/enter.png"  height="25" width="25"/>
											</h4>

											<div class="space-6"></div>

											<?php 
											if ($str_validate == FALSE){ echo validation_errors(); } 
											if ($this->session->flashdata('msg_error') != ''){ echo $this->session->flashdata('msg_error'); }
											?>

											
											<?php echo form_open("login/account", array('id'=>'login-form'));?>
												<fieldset>

													<section>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="txt_usr" value="<?php echo set_value('txt_usr');?>" class="form-control" placeholder="Username" />
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>
													</section>

													<section>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" name="txt_pwd" class="form-control" placeholder="Password" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>
													</section>

													<div class="space"></div>



														<button type="submit" class="width-100 pull-center btn btn-sm btn-primary" text-align: center>
															<i class="ace-icon fa fa-key bigger-130"></i>
															<span class="bigger-130">Login</span>
														</button>
													
													<div class="space-4"></div>

												</fieldset>

												
 
												<input type="hidden" name="action" value="<?php echo base64_encode('login');?>"  />
											<?php echo form_close();?>									
						</div>

								<div id="forgot-box" class="forgot-box widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header red lighter bigger">
												<i class="ace-icon fa fa-key"></i>
												Retrieve Password
											</h4>

											<div class="space-6"></div>
											<p>
												Enter your email and to receive instructions
											</p>

											<form>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="email" class="form-control" placeholder="Email" />
															<i class="ace-icon fa fa-envelope"></i>
														</span>
													</label>

													<div class="clearfix">
														<button type="button" class="width-35 pull-right btn btn-sm btn-danger">
															<i class="ace-icon fa fa-lightbulb-o"></i>
															<span class="bigger-110">Send Me!</span>
														</button>
													</div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->

										<div class="toolbar center">
											<a href="#" data-target="#login-box" class="back-to-login-link">
												Back to login
												<i class="ace-icon fa fa-arrow-right"></i>
											</a>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.forgot-box -->

								<div id="signup-box" class="signup-box widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header green lighter bigger">
												<i class="ace-icon fa fa-users blue"></i>
												New User Registration
											</h4>

											<div class="space-6"></div>
											<p> Enter your details to begin: </p>

											<form>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="email" class="form-control" placeholder="Email" />
															<i class="ace-icon fa fa-envelope"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" class="form-control" placeholder="Username" />
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="Password" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" class="form-control" placeholder="Repeat password" />
															<i class="ace-icon fa fa-retweet"></i>
														</span>
													</label>

													<label class="block">
														<input type="checkbox" class="ace" />
														<span class="lbl">
															I accept the
															<a href="#">User Agreement</a>
														</span>
													</label>

													<div class="space-24"></div>

													<div class="clearfix">
														<button type="reset" class="width-30 pull-left btn btn-sm">
															<i class="ace-icon fa fa-refresh"></i>
															<span class="bigger-110">Reset</span>
														</button>

														<button type="button" class="width-65 pull-right btn btn-sm btn-success">
															<span class="bigger-110">Register</span>

															<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
														</button>
													</div>
												</fieldset>
											</form>
										</div>

										<div class="toolbar center">
											<a href="#" data-target="#login-box" class="back-to-login-link">
												<i class="ace-icon fa fa-arrow-left"></i>
												Back to login
											</a>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.signup-box -->
							</div><!-- /.position-relative -->

							<!-- <div class="navbar-fixed-top align-right">
								<br />
								&nbsp;
								<a id="btn-login-dark" href="#">Dark</a>
								&nbsp;
								<span class="blue">/</span>
								&nbsp;
								<a id="btn-login-blur" href="#">Blur</a>
								&nbsp;
								<span class="blue">/</span>
								&nbsp;
								<a id="btn-login-light" href="#">Light</a>
								&nbsp; &nbsp; &nbsp;
							</div> -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<!-- <script src="assets/js/jquery-2.1.4.min.js"></script> -->

		<style>
		.hide-it{
			
		}
		.select-info{
			padding-left : 10px /*Data table row select */
		}
		.rotate-center {
			-webkit-animation: rotate-center 1s ease-in-out both;
			        animation: rotate-center 1s ease-in-out both;
		}
		.animation{
		  animation: expand_center 950ms;
		
		}
		@keyframes expand_center {
		  0% {
		  clip-path: polygon(50% 100%,50% 0,50% 0,50% 100%);
		    -webkit-clip-path: polygon(50% 100%,50% 0,50% 0,50% 100%);
		 }
		  100%{
		  clip-path: polygon(0 100%, 0 0, 100% 0, 100% 100%);
		    -webkit-clip-path: polygon(0 100%, 0 0, 100% 0, 100% 100%);
		  }
		}
		@-webkit-keyframes rotate-center {
		  0% {
		    -webkit-transform: rotate(0);
		            transform: rotate(0);
		  }
		  100% {
		    -webkit-transform: rotate(360deg);
		            transform: rotate(360deg);
		  }
		}
		@keyframes rotate-center {
		  0% {
		    -webkit-transform: rotate(0);
		            transform: rotate(0);
		  }
		  100% {
		    -webkit-transform: rotate(360deg);
		            transform: rotate(360deg);
		  }
		}



@media screen and (max-width: 600px) {
  table {
    border: 0;
  }
 	.nav-list>li>a>.menu-icon {
    font-weight: 900;
  }

  table caption {
    font-size: 1.3em;
  }
  
  table thead {
    border: none;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
  }
  
  table tr {
    border-bottom: 3px solid #ddd;
    display: block;
    margin-bottom: .625em;
  }
  
  table td {
    border-bottom: 1px solid #ddd;
    display: block;
    font-size: .8em;
    text-align: center;
  }
  
  table td::before {
    /*
    * aria-label has no advantage, it won't be read inside a table
    content: attr(aria-label);
    */
    content: attr(data-label);
    float: left;
    font-weight: bold;
    text-transform: uppercase;
  }
  
  table td:last-child {
    border-bottom: 0;
  }
}




	</style>
<script>
        $(document).ready(function() {
          $(".hide-it").fadeOut(5000);
});
   
    </script>