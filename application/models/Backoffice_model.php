<?php

class Backoffice_model extends CI_Model {

    public function Login($usr='',$pwd='') 
    {
		$p['usr'] = trim($usr);
		$p['pwd'] = base64_encode(trim($pwd));
		
		$sqlSel = "SELECT * FROM sys_users WHERE username='{$p['usr']}' and password='{$p['pwd']}';";
		$query = $this->db->query($sqlSel);
				
		if($query->num_rows()!=0) {
			
			$result = $query->result_array();
						
			if($result['0']['enable']!=0){
					
				return $result[0];
				
			}else{
				
				$error = array('action'=>'err', 'value'=>'b');			
				return $error;
			}
			
		}else { 
			
			$error = array('action'=>'err', 'value'=>'i');			
			return $error;
		}
		

    }
	public function modelDelUser($id){
		$sql ="DELETE from user where id ='{$id}'";
		$res =$this->db->query($sql);
		if($res){
			return true;
		}else{
			return false;
		}
	}
	public function modelGetUserById($id){
		$sql = "select * from user where id ='{$id}'";
		$res = $this->db->query($sql);
		$row = $res->result_array();
		return $row;
			
	}
	public function modelUpdateUser($id,$name,$pass){
		$sql = "update user set username ='{$name}', pass='{$pass}' where id ='{$id}'";
		$res = $this->db->query($sql);
		if($res){
			return true;
		}else{
			return false;	
		}

	}
	public function modelCheckLogin($name,$pass){
		$sql = "select * from user where username ='{$name}' and pass = '{$pass}'";
		$res = $this->db->query($sql);
		$row = $res ->result_array(); //แอร์โร เป็นการเข้าถึงข้อมูล ใช้เป็น OO
		
		if(empty($row)){
			return "There is no information in the system. Please enter the information again.";

		}else{
			return "Welcome  $name";
		}

	}
	public function modelGetuser(){
		$sql = "select * from user";
		$res = $this->db->query($sql);
		$row = $res->result_array();
		return $row;
	}
	public function modelCheckInsert($regisname,$regispass){
		

		$sql = "INSERT INTO `user`(username,pass) VALUES ('{$regisname}','{$regispass}')";
		$res = $this->db->query($sql);
	}

	public function ShowMenu($key){
		
		$sqlGroupMenu = "SELECT
		smg.name AS g_name,
		smg.icon_menu,
		sm.mg_id,
		smg.order_no
		FROM
		sys_menus sm 
		LEFT JOIN sys_menu_groups smg ON smg.mg_id = sm.mg_id
		WHERE sm.sp_id is NULL OR sm.sp_id IN (select sup.sp_id FROM sys_users_permissions AS sup WHERE sup.su_id = '$key' AND sm.enable = '1' AND smg.enable = '1')
		GROUP BY smg.name
						,smg.icon_menu
						,sm.mg_id
						,smg.order_no
		ORDER BY smg.order_no ASC ;";

		$excGroupMenu = $this->db->query($sqlGroupMenu);
		
		$result = array();
		foreach($excGroupMenu->result_array() as $gm){
			

			$sqlSubMenu = "SELECT
			sm.*
			FROM
			sys_menus AS sm
			LEFT JOIN sys_menu_groups AS smg ON smg.mg_id = sm.mg_id
			WHERE sm.mg_id='{$gm['mg_id']}' AND (sm.sp_id is NULL OR sm.sp_id IN (select sup.sp_id FROM sys_users_permissions AS sup WHERE sup.su_id = '$key'))
			ORDER BY smg.order_no ASC, sm.order_no ASC
			;";
			
			$excSubMenu = $this->db->query($sqlSubMenu);			
			
			$subMenu = array();	
			foreach($excSubMenu->result_array() as $sm){
				
				array_push($subMenu, array('name'=>$sm['name'], 'method'=>$sm['method'], 'link'=>$sm['link']));
				
			}
		
			array_push($result, array('g_name'=>$gm['g_name'],'icon_menu'=>$gm['icon_menu'],'sub_menu'=>$subMenu));
			
						
		}
		
		return $result;
		
	}

	public function Logout()
	{	
		/*$sqlDelFile = "DELETE FROM upload_temp_filename WHERE u_id='".$this->session->userdata('sessUsrId')."';";
		$excDelFile = $this->db->query($sqlDelFile);*/
		
		$sqlLastAcc = "UPDATE sys_users SET last_access=NOW() WHERE username='".$this->session->userdata('sessUsr')."';";
		$this->db->query($sqlLastAcc);
		$this->session->sess_destroy(); 
		
        redirect('login/account');
		
	}

	public function CheckPermission($para){
		
		$get_url = trim($this->router->fetch_class().'/'.$this->router->fetch_method());

		$sqlChkPerm = "SELECT
			sp.name,
			sp.controller
			FROM
			sys_users_permissions AS sup
			INNER JOIN sys_permissions AS sp ON sp.sp_id = sup.sp_id
			LEFT JOIN sys_permission_groups AS spg ON sp.spg_id = spg.spg_id
			WHERE
			sp.enable='1' AND spg.enable='1' AND sup.su_id='{$para}' AND sp.controller='{$get_url}';";
			
		$excChkPerm = $this->db->query($sqlChkPerm);
		$numChkPerm = $excChkPerm->num_rows();
		
		if($numChkPerm == 0) {
			
			echo '<script language="javascript">';
			echo 'alert("Permission not found.");';
			echo 'history.go(-1);';
			echo '</script>';
			exit();
			
		}

	}

	public function qc_data_json(){
		$this->qc = $this->load->database('qc_system', true);
		
		$sqlLoadData = "SELECT UNIX_TIMESTAMP(date_created) * 1000 as date_created ,data FROM qc_details WHERE data != 'NG' AND data != 'OK' ;";
		$excLoadData = $this->qc->query($sqlLoadData);
		//var_dump($excLoadData);
		$recLoadData = $excLoadData->result_array();
		$p = $recLoadData;
		
		return $p;
	}

	Public function qc_data(){
		$this->qc = $this->load->database('qc_system', true);
		
        $sqlLoadUser = "SELECT * FROM qc_details WHERE del_flag = '0' 
		AND DATE (date_created) = DATE (CURRENT_DATE()) AND YEAR (date_created) = YEAR (CURRENT_DATE());";
		$excLoadUser = $this->qc->query($sqlLoadUser);
		$recLoadUser = $excLoadUser->result_array();
		$data['list_qc'] = $recLoadUser;
		if ($data['list_qc'] != ''){
			return $data['list_qc'];
		}
		else{
			return False;	
			}
	}

	Public function test_json(){
		$this->qc = $this->load->database('qc_system', true);
		
        $sqlLoadUser = "SELECT qc_id, line_name, op_name, part_name, part_no, pro_no, item_no, shift, time, data, date_created FROM qc_details WHERE del_flag = '0' 
		;";
		$excLoadUser = $this->qc->query($sqlLoadUser);
		$recLoadUser = $excLoadUser->result_array();
		return $recLoadUser;
		// $data['list_qc'] = $recLoadUser;
		// if ($data['list_qc'] != ''){
		// 	return $data['list_qc'];
		// }
		// else{
		// 	return False;	
		// 	}
	}

	public function getname($uid=''){
		$sqlLoadData = "SELECT firstname FROM sys_users WHERE su_id = '{$uid}' ;";
		$excLoadData = $this->db->query($sqlLoadData);
		$recLoadData = $excLoadData->result_array();
		$result = $recLoadData;
			foreach ($result as $qc_detail){ 
			  return($qc_detail['firstname']);
			}
		//var_dump($result[0]);
	}

	public function qc_selectdate($mindate='', $maxdate=''){
		$this->qc = $this->load->database('qc_system', true);
		// SELECT qc_id, line_name, op_name, part_name, part_no, pro_no, item_no, shift, time, data, enable, date_created, leader_checked, emp_checked FROM qc_details 
		// WHERE del_flag = '0' AND date(date_created) BETWEEN '{$mindate}' AND '{$maxdate}' ;
		$sqlLoadData = "CALL SelectRangedate('$mindate', '$maxdate')";
		$excLoadData = $this->qc->query($sqlLoadData);
		$excLoad = $excLoadData->result_array();
		$data['list_qc'] = $excLoad;
		if ($data['list_qc'] != ''){
			return $data['list_qc'];
		}
		else{
			return False;	
			}

	}



	public function qc_delete($id){
		$this->qc = $this->load->database('qc_system', true);
		$sid = $this->session->userdata('sessUsrId');

		$sqlLoadData = "UPDATE qc_details SET del_flag = '1', enable = '0', del_date = NOW(), del_by = '{$id}';";
		$excLoadData = $this->qc->query($sqlLoadData);
		if ($excLoadData){
			return True;	
		}else{
			return False;
		}
		
	}

	public function getlinename(){
		$this->qc = $this->load->database('qc_system', true);
		$sqlSelG = "SELECT * FROM qc_linename WHERE del_flag='0' AND enable='1' ORDER BY line_name;";
		$data = $this->qc->query($sqlSelG);
		return $data;
	}

	public function getlinenameph10(){
		$this->qc = $this->load->database('qc_system', true);
		$sqlSelG = "SELECT * FROM qc_linename WHERE phase='10' AND del_flag='0' AND enable='1' ORDER BY line_name;";
		$data = $this->qc->query($sqlSelG);
		return $data;
	}

	public function getopname($lineid){
		$this->qc = $this->load->database('qc_system', true);
		$sqlLoadData = "SELECT op_id, op_name FROM qc_op WHERE op_lineid = '{$lineid}' AND del_flag = '0' AND ENABLE = '1';";
		$excLoadData = $this->qc->query($sqlLoadData);
		$recLoadData = $excLoadData->result_array();
		$result = $recLoadData;
		
		return $result;
	}

	public function getpartname($lineid){
		$this->qc = $this->load->database('qc_system', true);
		$sqlLoadData = "SELECT name_id, partname FROM qc_partname WHERE line_id = '{$lineid}' AND del_flag = '0' AND ENABLE = '1';";
		$excLoadData = $this->qc->query($sqlLoadData);
		$recLoadData = $excLoadData->result_array();
		$result = $recLoadData;
		
		return $result;
	}

	public function getprocessname($opid){
		$this->qc = $this->load->database('qc_system', true);
		$sqlLoadData = "SELECT pro_id, pro_no FROM qc_processno WHERE op_id = '{$opid}' AND del_flag = '0' AND ENABLE = '1';";
		$excLoadData = $this->qc->query($sqlLoadData);
		$recLoadData = $excLoadData->result_array();
		$result = $recLoadData;
		
		return $result;
	}

	public function getpartnumber($nameid){
		$this->qc = $this->load->database('qc_system', true);
		$sqlLoadData = "SELECT no_id, partno FROM qc_partno WHERE name_id = '{$nameid}' AND del_flag = '0' AND ENABLE = '1';";
		$excLoadData = $this->qc->query($sqlLoadData);
		$recLoadData = $excLoadData->result_array();
		$result = $recLoadData;
		
		return $result;
	}

	public function getitemnumber($proid){
		$this->qc = $this->load->database('qc_system', true);
		$sqlLoadData = "SELECT item_id, item_no FROM qc_itemno WHERE pro_id = '{$proid}' AND del_flag = '0' AND ENABLE = '1';";
		$excLoadData = $this->qc->query($sqlLoadData);
		$recLoadData = $excLoadData->result_array();
		$result = $recLoadData;
		
		return $result;
	}

	public function getqcdetails($lineid, $opid, $partnameid, $pronameid, $partnumid, $itemid, $mindate, $maxdate){
		$this->qc = $this->load->database('qc_system', true);
		$sqlLoadData = "SELECT COALESCE(UNIX_TIMESTAMP(qcd.leader_insert) * 1000 ,UNIX_TIMESTAMP(qcd.date_created) * 1000) as date_created ,data,
		ExtractNumber(txt10) as txt10, ExtractNumber(txt11) as txt11,ExtractNumber(txt7) as txt7,
		ExtractNumber(txt8) as txt8, ExtractNumber(txt9) as txt9
		FROM qc_details_master AS qcm
		LEFT JOIN qc_details AS qcd ON qcm.details_m_id = qcd.details_m_id
		WHERE line_id = '{$lineid}' AND op_id = '{$opid}' AND partno_id = '{$partnumid}' 
		AND item_id = '{$itemid}' AND partname_id = '{$partnameid}' AND pro_id = '{$pronameid}'
		AND DATA != 'NG' AND DATA != 'OK' AND DATA != '-'
		AND date(qcd.date_created) BETWEEN '{$mindate}' AND '{$maxdate}';";
		$excLoadData = $this->qc->query($sqlLoadData);
		$recLoadData = $excLoadData->result_array();
		$result = $recLoadData;
		
		return $result;
	}

    public function CheckSession() 
    {
        if($this->session->userdata('loggedIn')!="OK") {
					
           redirect('login/account');
		   return FALSE;
		   
        }else{	return TRUE; 	}
    }

	public function editqcdata($data, $id){
		$usrid = $this->session->userdata('sessUsrId');
		$this->qc = $this->load->database('qc_system', true);
		$sqlLoadData = "UPDATE qc_details SET DATA = '{$data}', date_edited = NOW(), 
		number_edited = number_edited + 1, edited_by = '{$usrid}'
		WHERE qc_id = '{$id}';";
		$excLoadData = $this->qc->query($sqlLoadData);
	}

	public function addUser($p){
		## User
		$setUser = array('sug_id' => $p['group'],
				'username' => $p['usr'],
				'password' => $p['pwd'],
				'firstname' => $p['fname'],
				'lastname' => $p['lname'],
				'gender' => $p['sex'],
				'email' => $p['email'],
				'enable' => '1',
				'date_created' => date('Y-m-d H:i:s'),
				'date_updated' => date('Y-m-d H:i:s'),	
				'del_flag' => '0',
				);
		
		$this->db->set($setUser);
		$excInsUser = $this->db->insert('sys_users');

		$lastId = $this->db->insert_id();
						
		## User Permission 
		$sqlSelPerm = "SELECT
			sp.sp_id,
			sp.`name`
			FROM
			sys_users_groups_permissions AS sugp
			LEFT JOIN sys_permission_groups AS spg ON spg.spg_id = sugp.spg_id
			LEFT JOIN sys_permissions AS sp ON sp.spg_id = spg.spg_id
			WHERE spg.`enable`='1' AND sp.`enable`='1' AND sugp.sug_id='{$p['group']}';";
		$excSelPerm = $this->db->query($sqlSelPerm);
		
		foreach($excSelPerm->result() AS $p){
			
			$setPerm = array('su_id' => $lastId,
					'sp_id' => $p->sp_id,
					'date_created' => date('Y-m-d H:i:s')
			);
			$this->db->set($setPerm);
			$excInsPerm = $this->db->insert('sys_users_permissions');
			/*var_dump($excInsUser);
			var_dump($excInsPerm);
			exit();*/
			
		}
			
			
				
		if($excInsUser && $excInsPerm ){ return $lastId; }else{ return FALSE; }
		
	}

/*	public function addUserProject($last_id,$pr_id){
		
		## User Project 
		$setUserPrj = array('su_id' => $last_id, 
					'pr_id' => $pr_id,
					'date_created' => date('Y-m-d H:i:s')			
					);
			
		$this->db->set($setUserPrj);
		$excInsUserPrj = $this->db->insert('user_project');
		
		if($excInsUserPrj){ return TRUE; }else{ return FALSE; }
		
	}*/

	

	public function deleteBom($key=''){
		
		$sqlDelBox = "DELETE FROM detail_bom WHERE bom_id='{$key}';";
		$excDelBox = $this->db->query($sqlDelBox);
				
		if($excDelBox) { return TRUE; }else { return FALSE; }
		
	}

	

	public function addFiletempname($name,$tempname){
		$setFiletempname = array('u_id' => $this->session->userdata('sessUsrId'),
				'file_name' => $name,
				'file_temp_name' => $tempname,
				'date_created' => date('Y-m-d H:i:s')				
				);
		
		$this->db->set($setFiletempname);
		$excInsUser = $this->db->insert('upload_temp_filename');
	}


	
	public function editUser($key,$p){
		
		if($p['pwd'] != ''){

			$setUser = array('sug_id' => $p['group'], 
				'password' => $p['pwd'],
				'firstname' => $p['fname'],
				'lastname' => $p['lname'],
				'gender' => $p['sex'],
				'email' => $p['email'],
				'date_updated' => date('Y-m-d H:i:s')				
				);
		}else{

			$setUser = array('sug_id' => $p['group'], 
				'firstname' => $p['fname'],
				'lastname' => $p['lname'],
				'gender' => $p['sex'],
				'email' => $p['email'],
				'date_updated' => date('Y-m-d H:i:s')				
				);
		}		
		
		$this->db->where('su_id',$key);
		$excUp = $this->db->update('sys_users',$setUser);
		
		if($excUp){ return TRUE; }else{ return FALSE; }
		
	}

/*	public function enableBox($key=''){
		
		$this->db->set('enable', '1');
		$this->db->set('date_updated', 'NOW()', FALSE);
		$this->db->where('box_id', $key);
		$exc_box = $this->db->update('box');
		
		if ($exc_box){
			
			return TRUE;	
		
		}else{	return FALSE;	}
		 
	}*/

/*	public function disableBox($key=''){
		
		$this->db->set('enable', '0');
		$this->db->set('date_updated', 'NOW()', FALSE);
		$this->db->where('box_id', $key);
		$exc_box = $this->db->update('box');
		
		if ($exc_box){
			
			return TRUE;	
		
		}else{	return FALSE;	}
		 
	}*/

	public function num_enableBox($para){
		
		for($i=0;$i<count($para);$i++){
			
			$this->enableBox($para[$i]);
																									
		}
		
		return TRUE;
		
	}

	public function num_disableBox($para){

		for($i=0;$i<count($para);$i++){
			
			$this->disableBox($para[$i]);
																									
		}
		
		return TRUE;
		
	}

	public function num_deleteBox($para){
		
		for($i=0;$i<count($para);$i++){
			
			$this->deleteBox($para[$i]);
																									
		}
		
		return TRUE;
		
	}
	
	public function enableUser($key=''){
		
		$this->db->set('enable', '1');
		$this->db->set('date_updated', 'NOW()', FALSE);
		$this->db->where('su_id', $key);
		$exc_user = $this->db->update('sys_users');
		
		if ($exc_user){
			
			return TRUE;	
		
		}else{	return FALSE;	}
		 
	}

	public function num_enableUser($para){
		
		for($i=0;$i<count($para);$i++){
			
			$this->enableUser($para[$i]);
																									
		}
		
		return TRUE;
		
	}

	public function deleteqcdetail($key=''){
		$sid = $this->session->userdata('sessUsrId');
		$this->qc = $this->load->database('qc_system', true);
		$sqlDel = "UPDATE qc_details SET del_flag = '1', del_date = NOW(), del_by = '{$sid}'  WHERE qc_id='{$key}';";
		$excDel = $this->qc->query($sqlDel);
				
		if($excDel) { return TRUE; }else { return FALSE; }
		
	}

	public function check_data($key=''){
		$this->qc = $this->load->database('qc_system', true);
		//$this->qc->set('checked_data', '1');
		$this->qc->set('check_date', 'NOW()', FALSE);
		$this->qc->set('leader_checked',$this->session->userdata('sessUsrId'));
		$this->qc->where('qc_id', $key);
		$exc_user = $this->qc->update('qc_details');
		
		if ($exc_user){
			
			return TRUE;	
		
		}else{	return FALSE;	}
		 
	}

	public function num_check_data($id){
		for($i=0;$i<count($id);$i++){
			
			$this->check_data($id[$i]);
																									
		}
		
		return TRUE;
	}
	public function disableUser($key=''){
		
		$this->db->set('enable', '0');
		$this->db->set('date_updated', 'NOW()', FALSE);
		$this->db->where('su_id', $key);
		$exc_user = $this->db->update('sys_users');
		
		if ($exc_user){
			
			return TRUE;	
		
		}else{	return FALSE;	}
		 
	}

	public function num_disableUser($para){

		for($i=0;$i<count($para);$i++){
			
			$this->disableUser($para[$i]);
																									
		}
		
		return TRUE;
		
	}

	public function deleteUser($key=''){
		$sid = $this->session->userdata('sessUsrId');
		$this->db->set('enable', '0');
		$this->db->set('del_flag', '1');
		$this->db->set('date_deleted', 'NOW()', FALSE);
		$this->db->set('del_by', $sid);
		$this->db->where('su_id', $key);
		$exc_user = $this->db->update('sys_users');

		if ($exc_user){
			
			return TRUE;	
		
		}else{	return FALSE;	}
		
	}

	
	public function num_deleteUser($para){
		
		for($i=0;$i<count($para);$i++){
			
			$this->deleteUser($para[$i]);
																									
		}
		
		return TRUE;
		
	}

	
	public function ShowUser($key=''){
		
		$sqlSel = "SELECT * FROM sys_users WHERE su_id='{$key}';";
		$query = $this->db->query($sqlSel);
		$result = $query->result_array();
		
		if($query->num_rows()!=0) {
			
            return $result[0];
        }else { 
		
            return FALSE;
        }
		
	}
		
	public function searchUser($searchTerm=''){
		
		
		 $this->db->select('*');
		 $this->db->from('sys_users');
		 $this->db->where('sug_id !=', '1');
		 $this->db->like('username',$searchTerm);
		 $this->db->or_like('firstname',$searchTerm);
		 $this->db->or_like('lastname',$searchTerm);
		 $this->db->or_like('email',$searchTerm);
		 
		 $query = $this->db->get();
		 
		 return $query->result_array();
		
	}
	
	public function getUser($str){
		
        $this->db->where('username', $str);
        $query = $this->db->get('sys_users');
        
		if($query->num_rows() > 0){
			
			return FALSE;
		}else{
			
			return TRUE;	
		}
		
    }	

	public function AddUserGroup($name='',$enable='1'){
		
		$sqlIns = "INSERT INTO sys_user_groups SET name='{$name}', enable='{$enable}', date_created=NOW(), del_flag='0'";
		$excIns = $this->db->query($sqlIns);
		
		if($excIns){ return TRUE; }else{ return FALSE; }
		
		
	}

	public function EditUserGroup($key='',$name='',$enable='1'){
		
		$sqlEdt = "UPDATE sys_user_groups SET name='{$name}', enable='{$enable}', date_created=NOW() WHERE sug_id='{$key}';";
		$excEdt = $this->db->query($sqlEdt);
		
		if($excEdt){ return TRUE; }else{ return FALSE; }
		
		
	}

	public function DeleteUserGroup($key=''){
		$sid = $this->session->userdata('sessUsrId');
		$sqlDel = "UPDATE sys_user_groups SET del_by='{$sid}', enable='0', del_flag='1', date_deleted=NOW() WHERE sug_id='{$key}';";
		$excDel = $this->db->query($sqlDel);
		
		$sqlDel = "UPDATE sys_users_groups_permissions SET del_by='{$sid}', del_flag='1', date_deleted=NOW() WHERE sug_id='{$key}';";
		$excDel = $this->db->query($sqlDel);
		
		if($excDel) { return TRUE; }else { return FALSE; }
		
	}

	public function ShowUserGroup($key=''){
		
		$sqlSel = "SELECT * FROM sys_user_groups WHERE sug_id='{$key}';";
		$query = $this->db->query($sqlSel);
		$result = $query->result_array();
		
		if($query->num_rows()!=0) {
			
            return $result[0];
        }else { 
		
            return FALSE;
        }
		
	}

	public function AddRuleUserGroup($key,$rule=''){
		
		$sqlDel = "DELETE FROM sys_users_groups_permissions WHERE sug_id='{$key}';";
		$excDel = $this->db->query($sqlDel);
		
		if($rule!=''){
		
			foreach($rule as $r){
				
				$sqlIns = "INSERT INTO sys_users_groups_permissions SET sug_id='{$key}', spg_id='{$r}', date_created=NOW(), del_flag = '0' ;";
				$excIns = $this->db->query($sqlIns);
				
				if($excIns){
					
					$strIns = TRUE;
					
				}else{ $strIns = FALSE;}
			}
		
		}else{ $strIns = TRUE; }
		
		return $strIns;
		
		
	}

	public function AddRuleUser($key,$rule=''){
		
		$sqlDel = "DELETE FROM sys_users_permissions WHERE su_id='{$key}';";
		$excDel = $this->db->query($sqlDel);
		
		if($rule!=''){
		
			foreach($rule as $r){
				
				$sqlIns = "INSERT INTO sys_users_permissions SET su_id='{$key}', sp_id='{$r}', date_created=NOW();";
				$excIns = $this->db->query($sqlIns);
				
				if($excIns){
					
					$strIns = TRUE;
					
				}else{ $strIns = FALSE;}
			}
		
		}else{ $strIns = TRUE; }
		
		return $strIns;
		
		
	}

	## WHERE ID USER GROUP ##
	public function AddRuleUserDefault($key){
		
		$sqlSelUserG = "SELECT
		DISTINCT sup.su_id AS su_id
		FROM
		sys_users AS su
		LEFT JOIN sys_users_permissions AS sup ON sup.su_id = su.su_id
		WHERE 
		su.sug_id='{$key}';";


		$excSelUserG = $this->db->query($sqlSelUserG);
				
		foreach($excSelUserG->result_array() AS $r){
			
			$uId .= "'".$r['su_id']."',";								
		}
				
		$sqlDel = "DELETE FROM sys_users_permissions WHERE su_id IN ({$uId}FALSE);";
		$excDel = $this->db->query($sqlDel);
		
		if($excDel){
		
			$sqlSelPerDefault = "INSERT INTO sys_users_permissions 
			SELECT
			NULL AS sup_id,
			su.su_id,
			sp.sp_id,
			NOW()
			FROM
			sys_users_groups_permissions AS sugp
			LEFT JOIN sys_user_groups AS sug ON sug.sug_id = sugp.sug_id
			LEFT JOIN sys_users AS su ON su.sug_id = sug.sug_id
			LEFT JOIN sys_permission_groups AS spg ON spg.spg_id = sugp.spg_id
			LEFT JOIN sys_permissions AS sp ON sp.spg_id = spg.spg_id
			WHERE sugp.sug_id='{$key}'
			ORDER BY su.su_id ASC,sp.sp_id ASC";
			$excSelPerDefault = $this->db->query($sqlSelPerDefault);
		
			if($excSelPerDefault){
				
				return TRUE;
					
			}else{ return FALSE; }
		
		}else{
		
			return FALSE;	
		}
	
	}

	public function AddPermission($name='',$enable='1',$group, $cont){
		
		$sqlIns = "INSERT INTO sys_permissions SET name='{$name}', controller='{$cont}',enable='{$enable}', spg_id='{$group}',date_created=NOW(), date_updated=NOW(), del_flag = '0';";
		$excIns = $this->db->query($sqlIns);
		
		if($excIns){ return TRUE; }else{ return FALSE; }
		
		
	}

	public function EditPermission($key='',$name='',$enable='1',$group, $cont){
		
		$sqlEdt = "UPDATE sys_permissions SET name='{$name}', controller='{$cont}', enable='{$enable}', spg_id='{$group}', date_updated=NOW() WHERE sp_id='{$key}';";
		$excEdt = $this->db->query($sqlEdt);
		
		if($excEdt){ return TRUE; }else{ return FALSE; }
		
		
	}
	
	public function DeletePermission($key=''){
		$sid = $this->session->userdata('sessUsrId');
		$sqlDel = "UPDATE sys_permissions SET del_by='{$sid}', enable='0', del_flag='1', del_by='{$sid}', date_deleted=NOW() WHERE sp_id='{$key}';";
		$excDel = $this->db->query($sqlDel);
		
		if($excDel) { return TRUE; }else { return FALSE; }
		
	}
	
	public function ShowPermission($key=''){
		
		$sqlSel = "SELECT * FROM sys_permissions WHERE sp_id='{$key}';";
		$query = $this->db->query($sqlSel);
		$result = $query->result_array();
		
		if($query->num_rows()!=0) {
			
            return $result[0];
        }else { 
		
            return FALSE;
        }
		
	}


	public function AddPermissionGroup($name='',$enable='1'){
		
		$sqlIns = "INSERT INTO sys_permission_groups SET name='{$name}', enable='{$enable}', date_created=NOW(), del_flag='0';";
		$excIns = $this->db->query($sqlIns);
		
		if($excIns){ return TRUE; }else{ return FALSE; }
		
		
	}

	public function EditPermissionGroup($key='',$name='',$enable='1'){
		
		$sqlEdt = "UPDATE sys_permission_groups SET name='{$name}', enable='{$enable}', date_created=NOW() WHERE spg_id='{$key}';";
		$excEdt = $this->db->query($sqlEdt);
		
		if($excEdt){ return TRUE; }else{ return FALSE; }
		
		
	}

	public function DeletePermissionGroup($key=''){
		$sid = $this->session->userdata('sessUsrId');
		$sqlDel = "UPDATE sys_permission_groups SET del_by='{$sid}', enable='0', del_flag='1', date_deleted=NOW() WHERE spg_id='{$key}';";
		$excDel = $this->db->query($sqlDel);
		
		if($excDel) { return TRUE; }else { return FALSE; }
		
	}

	public function ShowPermissionGroup($key=''){
		
		$sqlSel = "SELECT * FROM sys_permission_groups WHERE spg_id='{$key}';";
		$query = $this->db->query($sqlSel);
		$result = $query->result_array();
		
		if($query->num_rows()!=0) {
			
            return $result[0];
        }else { 
		
            return FALSE;
        }
		
	}


	public function ShowBom($key=''){
		
		$sqlSel = "SELECT * FROM detail_bom WHERE bom_id='{$key}';";
		$query = $this->db->query($sqlSel);
		$result = $query->result_array();
		
		if($query->num_rows()!=0) {
			
            return $result[0];
        }else { 
		
            return FALSE;
        }
		
	}

	


	public function apisearch_bom($item_cd='',$parent_cd=''){
		// create & initialize a curl session
		$curl = curl_init();
		$data = array('code' => $item_cd,'parent' => $parent_cd);

		// set our url with curl_setopt()
		curl_setopt($curl, CURLOPT_URL, "http://192.168.161.102/exp_api3party/Test/search_BOM");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		// return the transfer as a string, also with setopt()
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		// curl_exec() executes the started curl session
		$output = curl_exec($curl);
		// close curl resource to free up system resources
		// (deletes the variable made by curl_init)
		curl_close($curl);
		return $output;
	}

	public function AddBom($COMP_ITEM_CD='', $PARENT_ITEM_CD='', $PS_UNIT_DENOMINATOR='', $PS_UNIT_NUMERATOR='' , $enable='1'){
		
		$sqlIns = "INSERT INTO detail_bom (comp_code, parent_code, unit_denominator,unit_numerator,enable) VALUES ('$COMP_ITEM_CD', '$PARENT_ITEM_CD', '$PS_UNIT_DENOMINATOR', '$PS_UNIT_NUMERATOR', '$enable');";
		$excIns = $this->db->query($sqlIns);

		
		if($excIns){ return TRUE; }else{ return FALSE; }


	}

		public function EditBom($key='', $comp_code='' , $enable=''){

		$sqlEdt = "UPDATE detail_bom SET comp_code='{$comp_code}', enable='{$enable}'  WHERE bom_id='{$key}';";
		$excEdt = $this->db->query($sqlEdt);
		
		if($excEdt){ return TRUE; }else{ return FALSE; }
		
		
	}





	
	public function editProfile($key,$p){
		
		$setUser = array('firstname' => $p['fname'],
				'lastname' => $p['lname'],
				'gender' => $p['sex'],
				'email' => $p['email'],
				'date_updated' => date('Y-m-d H:i:s')				
				);
		
		$this->db->where('su_id',$key);
		$excUp = $this->db->update('sys_users',$setUser);
		
		if($excUp){ return TRUE; }else{ return FALSE; }


		
	}	
	

    public function changePassword($key,$p){
		
        $this->db->where('su_id', $key);
        $this->db->where('password', base64_encode(trim($p['oldPwd'])));
        $query = $this->db->get('sys_users');
		
        if($query->num_rows()==1){
			
            $data = array('password' => base64_encode(trim($p['newPwd'])),
						'date_updated' => date('Y-m-d H:i:s')
			); 
            $this->db->where('su_id', $key);
            
			$query = $this->db->update('sys_users', $data); 
			
            if($query) {
				
                return TRUE;
            } else {
				
                return FALSE;
            }
        } else {
			
            return FALSE;// data not found
        }   
    }

    public function sendEmail($mailData){
 		


       	$this->load->library('email');

		$config['charset']='utf-8';
		$config['newline']="\r\n";
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		$this->email->initialize($config);
		$this->email->from('issue@tbkk.co.th', 'issue_system');
		$this->email->to('wisit_t@tbkk.co.th');
		// $this->email->cc('saranee@tbkk.co.th');
		$this->email->subject('test new system email');
		$this->email->message('test new system email krab');
		$this->email->send();

        // ส่งจากอีเมล - ชื่อ
//         $this->email->from($mailData['mail_email'],$mailData['mail_name']); 
//         $this->email->to('wisit_t@tbkk.co.th','wisit_t'); // ส่งถึง
         
//         // หากต้องการส่งแบบให้มี cc หรือ bcc กำหนดตามด้านล่าง
// //        $this->email->cc('another@another-example.com');
// //        $this->email->bcc('them@their-example.com');        
         
//         // หากต้องการแนบไฟล์ กำหนดตามนี้
// //        $this->email->attach('/path/to/photo1.jpg');
// //        $this->email->attach('/path/to/photo2.jpg');
// //        $this->email->attach('/path/to/photo3.jpg');        
//         // หรือ แนบไฟล์แบบให้แสดงในอีเมลเลย เหมาะกับรูป
// //        $this->email->attach('image.jpg', 'inline');
//         // หรือ แนบไฟล์แบบกำหนด url เข้าไปตรงๆ เลย
// //        $this->email->attach('http://www.ninenik.com/filename.pdf');
//         // หรือแบบอัพโหลดไฟล์
// //        $this->email->attach($buffer, 'attachment', 'report.pdf', 'application/pdf');
 		
//         $this->email->subject($mailData['mail_title']); // หัวข้อที่ส่ง
//         $this->email->message($mailData['mail_detail']);  // รายละเอียด
//         return $this->email->send();   // คืนค่าการทำงานว่าเป็น true หรือ false      
         
    }
	
	
	
	/*
	

	public function resetpassword(){
		
		
		$this->db->where('m_usr', $this->session->userdata('sess_usr'));
		$this->db->where('m_email', trim($this->input->post('txt_email')));
        $query = $this->db->get('member');			
					
        if($query->num_rows()==1){
			
			$newPwd = $this->random_text(6);
			$rec = $query->row_array();
			$data = array('m_pwd' => base64_encode($newPwd), 'm_updated_at' => date('Y-m-d H:i:s')); 
            $this->db->where('m_usr', $this->session->userdata('sess_usr'));
            
			$excQuery = $this->db->update('member', $data); 
			
            if($excQuery) {
				
				//-----------Message-------------------
				$mail_msg = "<b>เรียน สมาชิก 3BBWiFi Report Manager</b><br><br>";
				$mail_msg .= "ขอแจ้งรายละเอียด New Reset Password คือ<br><br>";
				$mail_msg .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Username : <font color='#0000FF'>".$this->session->userdata('sess_usr')."</font><br>";
				$mail_msg .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Password : <font color='#0000FF'>".$newPwd."</font><br><br>";
				$mail_msg .= "เพื่อความปลอดภัยของท่าน กรุณาทำการเปลี่ยนรหัสผ่านใหม่อีกครั้ง เมื่อทำการเข้าสู่ระบบ<br><br>";
				$mail_msg .= "กรุณาตรวจสอบข้อมูล เพื่อความถูกต้องค่ะ<br><br>";
				$mail_msg .= "ขอบคุณค่ะ<br>";
	
				//------------------------------------------	
		
				$mail_subject = "Reset password 3BBWiFi Report Manager";

				$this->send_mail($rec['m_email'], $mail_msg, $mail_subject);
                return TRUE;
				
            } else {
				
                return FALSE;
            }
			
		}else{
			return FALSE;// data not found	
		}
	}


	public function send_mail($email_usr,$mail_msg,$mail_subject){ //mail server 3bb vas
	
	//require_once( $path ); // Mail Class
	require_once('phpmailer/class.phpmailer.php');
	//---------------------------------------------------------------------
	$mail = new PHPMailer();
			
	$mail->IsSMTP();			// send via SMTP
	$mail->Host = "110.164.76.67";	// SMTP servers
    $mail->SMTPAuth = true; 		// turn on SMTP authentication
	$mail->Username = "system"; 	// SMTP username
	$mail->Password = "acumenit"; 	// SMTP password
	$mail->Mailer = "smtp";
	$mail->Port = '10025';	
	$mail->Priority = 1;
	$mail->Encoding = "8bit";
	$mail->CharSet = "utf-8";
	$mail->SMTPDebug = 1;	
	$mail->From = "system@3bbwifibackoffice.com"; // ชื่อเมล์ที่ถูกส่งออกไป
	$mail->FromName = "3BBWiFi Backoffice";
	$mail->AddAddress($email_usr); 	// email1 = ชื่อเมล์ที่ต้องการรับ 

			
	$mail->WordWrap = 80; 	// set word wrap
	$mail->IsHTML(true); 	// send as HTML
			
	$mail->Subject  =  $mail_subject;
	$mail->Body = $mail_msg;
			
	//###  ตรวจสอบการส่งเมล์ ของ Mail Server
	if( !$mail->Send() ) {
		$sendFlag = 0;

	} else { 
		$sendFlag = 1;

	}	
	
	//###  Clear all addresses and attachments for next loop
	$mail->ClearAddresses();
	$mail->ClearAttachments();
	$mail->ClearCCs();
			
	unset($mail);
	$mail_msg = "";
	unset($mail_msg);
	
	}

	public function random_text($len){
		srand((double)microtime()*10000000);
		$chars = "acdfhjkmnprtuvwxyz123456789";
		$ret_str = "";
		$num = strlen($chars);
		
		for($i = 0; $i < $len; $i++){
			$ret_str.= $chars[rand()%$num];
			$ret_str.="";
		}
		return $ret_str;
	}*/


}
?>
